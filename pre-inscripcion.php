<?php 
session_start();
if(!isset($_SESSION['codigo'])){
	header('Location: validar_codigo.php');
}



?>


<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Pre-Matricula</title>
<link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="lib/fontawesome/css/all.css">
    <link rel="stylesheet" href="styles/all.css">
		
	</head>
	<body>
		    <div class="container-fluid">
      <div class="row">
        <?php include("sections/menu.html");?>
      </div>
    </div>
		<div class="parallax" data-parallax="scroll" data-image-src="images/bg-titles-page.png">
      <h1 class="parallax-title text-center py-5 text-shadow"><b>FORMULARIO DE PRE - INSCRIPCIÓN</b></h1>
    </div>

<div class="container">
			<form action="BBDD/Datosf" method="POST" role="form">
				<div class="form-group">
	<div class="row">

		<?php  include('sections/formulario-pre-inscripcion.php'); ?>
		
	</div>
	</div>
</form>
</div>


						

     <div class="container-fluid">
    <!-- Inicio footer-->
    <div class="row footer">
      <?php include ("sections/footer.html");?>
    </div>
  </div>
	</body>
		<!-- jQuery -->
		<script src="js/jquery.js"></script>
  <script src="lib/parallax.js-1.5.0/parallax.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
		<!-- Bootstrap JavaScript -->
		
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->




</html>