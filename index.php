<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="styles/index_css.css">
		<link rel="stylesheet" href="styles/carousel_css.css">
		<link rel="stylesheet" href="lib/fontawesome/css/all.css">
		<link rel="stylesheet" href="styles/all.css">
		<title>Inicio</title>
	</head>
	<body>
		<div id="fb-root"></div>
		<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v5.0"></script>
		<div class="container-fluid">
			<div class="row">
				<?php include ("sections/menu.html");?>
			</div>
		</div>
		<div class="container-fluid">
			<!-- Inicio Carrousel-->
			<?php include ("sections/carousel2.html");?>
		</div>
		<!-- Inicio mensaje-->
		<div class="container-fluid bg_bienvenida bg-pr">
			<?php include ("sections/index_mensaje.html");?>
			
		</div>
		<div class="container-fluid py-5 text-white text-center" id="bg-himno">
			<div class="container ">
				<h2 class="text-center pb-4 ">Himno del Instituto Carmen Conte Lombardo</h2>
				<div class="row ">
					<div class="col-xs-12 mx-auto">
						<audio controls="">
							<source src="lib/himno-ccl.ogg">
							<source src="lib/himno-ccl.mp3">
						</audio>
						
						<p>(CORO)</p>
						<b><p><i>Gloria, ciencia y estudio seguimos,
							<br>Con empeños y fe sin igual <br>
							La victoria y el bien perseguimos <br>
						Del preciado Instituto Rural.</i></p></b>

						<p><i>A las cumbres altivo ilumina <br>
							Cual antorcha de luz esplendente <br>
							Este centro que al campo encamina <br>
						Dando brío y vigor a la mente.</i></p>
						<p>(CORO)</p>
						<p><i>La maestra tenaz Carmen Conte <br>
							Su importancia vital comprendió <br>
							Fue gran líder del amplio horizonte <br>
						Por sus luchas el triunfo alcanzó.</i></p>
						<p>(CORO)</p>
						<p><i>Son los valles, los bosques, los prados <br>
							Fuerzas vivas que dan al país <br>
							La riqueza y progreso anhelado <br>
						De un futuro brillante y feliz. </i></p>
						<p>(CORO)</p>
					</div>
				</div>
			</div>
		</div>
		<div class="container py-3 bg-pr">
			<div class="fb-page" data-href="https://www.facebook.com/Instituto.Carmen.Conte.Lombardo/" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/Instituto.Carmen.Conte.Lombardo/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Instituto.Carmen.Conte.Lombardo/">Instituto Carmen Conte Lombardo</a></blockquote></div>
		</div>
		
		<!-- Inicio footer-->
		<div class="container-fluid">
			<div class="row footer">
				<?php include ("sections/footer.html");?>
			</div>
		</div>
		
		
		<script src="js/jquery.js"></script>
		<script src="lib/bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>