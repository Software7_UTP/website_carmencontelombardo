<?php

include 'conexion.php';
include '../Entidades/Usuario.php';
class UsuarioDao extends conexion{
	protected static $cnx;
	 private static function  getConexion(){
	 	self::$cnx =conexion::conectar();
	 }
private static function desconectar(){
	self::$cnx =null;
}


//metodo para validar el login
public static function login($usuario){

	$query = "SELECT * FROM usuario1 WHERE  usuario=:usuario AND password=:password";

self::getConexion();

$resultado= self::$cnx->prepare($query);
$resultado->bindValue(":usuario",$usuario->getUsuario());
$resultado->bindValue(":password",$usuario->getPassword());

$resultado->execute();
 $count = $resultado->rowCount(); 

if($count >0){
	$filas=$resultado->fetch();
	if($filas["usuario"]==$usuario->getUsuario()&& $filas["password"]==$usuario->getPassword()){
return true;
	}
}
return false;
}


//metodo para obtener un usuario
public static function getUsuario($usuario){

	$query = "SELECT id,usuario,apellido,correo,telefono,tipodeusuario FROM usuario1 WHERE  usuario=:usuario AND password=:password";

self::getConexion();

$resultado= self::$cnx->prepare($query);
$resultado->bindValue(":usuario",$usuario->getUsuario());
$resultado->bindValue(":password",$usuario->getPassword());

$resultado->execute();

$filas=$resultado->fetch();

$usuario= new Usuario();
$usuario->setId($filas["id"]);
$usuario->setUsuario($filas["usuario"]);
$usuario->setApellido($filas["apellido"]);
$usuario->setCorreo($filas["correo"]);
$usuario->setTelefono($filas["telefono"]);
$usuario->setTipodeusuario($filas["tipodeusuario"]);

return $usuario;


}

}