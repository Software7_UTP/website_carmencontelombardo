<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="lib/fontawesome/css/all.css">
		<link rel="stylesheet" href="styles/all.css">
		<link rel="stylesheet" href="styles/bachiller-turismo.css">
		<link rel="stylesheet" href="lib/baguetteBox/css/baguetteBox.min.css">
		<title>Bachiller en Turismo</title>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<?php include ("sections/menu.html");?>
			</div>
		</div>
		<!-- Titulo-->
		<div class="parallax" data-parallax="scroll" data-image-src="images/bg-titles-page.png">
			<h1 class="parallax-title text-center py-5 text-shadow  animated fadeIn"><b>BACHILLER EN TURISMO</b></h1>
		</div>
		<!-- Contenido -->
		<div class="container-fluid pb-4 bg-pr">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 pl-0 pr-0 pt-3">
						<img class="img-fluid img-shadow" src="images/img-bachiller-turismo/guia-turistico-practica.jpg" >
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
						<h2 class="text-gray text-section-title py-3 ">En el año 2010 se abre el Bachiller en Turismo con la finalidad de dar respuesta a las exigencias del mercado laboral de la provincia.</h2>
					</div>
				</div>
			</div>
		</div>
		<div class="parallax" data-parallax="scroll" id="bg-green-primary">
						<div class="container text-white">
				<h2 class="text-center py-5">En este bachillerato se conjuga continuamente la teoría y la práctica.</h2>
				<p class="text-center">A través de esta asignatura los estudiantes del bachiller adquieren conocimientos básicos que le permiten desenvolverse en el campo laboral.</p>
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-6 text-white">
				</div>
				<div class="row py-3">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 my-auto">
						<img class="img-fluid img-shadow mb-3" src="images/img-bachiller-turismo/turismo-exposicion-2.jpg" id="img-coctel">
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 my-auto">
						<img class="img-fluid img-shadow mb-3" src="images/img-bachiller-turismo/turismo-exposicion.jpg" id="img-coctel">
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 my-auto">
						<img class="img-fluid img-shadow mb-3" src="images/img-bachiller-turismo/turismo-exposicion-3.jpg" id="img-coctel">
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid pt-5  pb-4 text-gray bg-pr">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-xs-12 col-sm-12 col-md-12 offset-lg-3 col-lg-6">
						<div class="container">
							<h3 class="text-center" >Clases de Servicio Turístico 12</h3>
							<p class="text-center">Estudiantes del  Bachiller en Turismo aprendiendo sobre cristalería y cubertería y en clases de gatronomía.</p>
							<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
								<ol class="carousel-indicators">
									<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
									<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
									<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
								</ol>
								<div class="carousel-inner">
									<div class="carousel-item active ">
										<img class="d-block w-100" src="images/img-bachiller-turismo/clases-de-gastronomia-2.jpg" alt="gastronomía">
										
									</div>
									<div class="carousel-item ">
										<img class="d-block w-100" src="images/img-bachiller-turismo/clases-de-gastronomia.jpg" alt="gastronomía">
									</div>
									<div class="carousel-item">
										<img class="d-block w-100" src="images/img-bachiller-turismo/clases-de-gastronomia-3.jpg" alt="gastronomía">
									</div>
								</div>
								<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
									<span class="carousel-control-prev-icon" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
								</a>
								<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
									<span class="carousel-control-next-icon" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<h3 class="text-center mt-5">Clases de Turismo Sostenible y Giras Académicas</h3>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<img class="img-fluid" src="images/img-bachiller-turismo/estudiantes-turismo-sostenible-2.jpg">
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
							</ol>
							<div class="carousel-inner">
								<div class="carousel-item active ">
									<img class="d-block w-100" src="images/img-bachiller-turismo/giras-educativas-las-tablas.jpg" alt="gastronomía">
								</div>
								<div class="carousel-item ">
									<img class="d-block w-100" src="images/img-bachiller-turismo/giras-educativas.jpg" alt="gastronomía">
								</div>
								<div class="carousel-item ">
									<img class="d-block w-100" src="images/img-bachiller-turismo/giras-educativas-cocoli.jpg" alt="gastronomía">
								</div>
								
							</div>
							<a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<div class="container-fluid py-5 text-gray bg-pr">
			<div class="container">
				<h2 class=" text-center">Celebraciones organizadas por el Bachiller en Turismo</h2>
				<h3 class="pt-5 text-center">Día de la Etnia Negra</h3>
				<p class="text-center">Los Estudiantes presentan murales, lucen diferentes vestidos alucivos y realizan concursos.</p>
				<div class="row">
					<div class="galeria-imagenes">
						<div class="row">
							<div class="col-sm-12 col-md-4">
								<a class="lightbox" href="images/img-bachiller-turismo/etnia-negra-congo.jpg">
									<img src="images/img-bachiller-turismo/etnia-negra-congo.jpg" alt="Etnia Negra">
								</a>
							</div>
							<div class="col-sm-6 col-md-4">
								<a class="lightbox" href="images/img-bachiller-turismo/etnia-negra-2.jpg">
									<img src="images/img-bachiller-turismo/etnia-negra-2.jpg" alt="Estudiantes luciendo diferentes vestidos representativos de la etnia Negra">
								</a>
							</div>
							<div class="col-sm-6 col-md-4">
								<a class="lightbox" href="images/img-bachiller-turismo/etnia-negra.jpg">
									<img src="images/img-bachiller-turismo/etnia-negra.jpg">
								</a>
							</div><div class="col-sm-6 col-md-4">
							<a class="lightbox" href="images/img-bachiller-turismo/etnia-negra-zaracunde.jpg">
								<img src="images/img-bachiller-turismo/etnia-negra-zaracunde.jpg" alt="Estudiantes con vestidos de Zaracundé">
							</a>
						</div>
						<div class="col-sm-6 col-md-4">
							<a class="lightbox" href="images/img-bachiller-turismo/etnia-negra-congo-2.jpg">
								<img src="images/img-bachiller-turismo/etnia-negra-congo-2.jpg" alt="Concurso de sombreros">
							</a>
						</div>
						<div class="col-sm-6 col-md-4">
							<a class="lightbox" href="images/img-bachiller-turismo/etnia-negra-mural.jpg">
								<img src="images/img-bachiller-turismo/etnia-negra-mural.jpg" >
							</a>
						</div>
					</div>
				</div>
			</div>
			<h3 class="pt-5 text-center">Día del Turismo</h3>
			<p class="text-center">Resaltando las costumbres y tradiciones de nuestro país.</p>
			<div class="row">
				<div class="galeria-imagenes">
					<div class="row">
						<div class="col-sm-6 col-md-4">
							<a class="lightbox" href="images/img-bachiller-turismo/dia-del-turismo.jpg">
								<img src="images/img-bachiller-turismo/dia-del-turismo.jpg" >
							</a>
						</div>
						<div class="col-sm-6 col-md-4">
							<a class="lightbox" href="images/img-bachiller-turismo/dia-del-turismo-3.jpg">
								<img src="images/img-bachiller-turismo/dia-del-turismo-3.jpg">
							</a>
						</div>
						<div class="col-sm-12 col-md-4">
							<a class="lightbox" href="images/img-bachiller-turismo/dia-del-turismo-2.jpg">
								<img src="images/img-bachiller-turismo/dia-del-turismo-2.jpg" alt="Estudiantes participantes del Concurso Chica y Chico Turismo 2017">
							</a>
						</div>
					</div>
					<p class=" my-4 text-center">Los estudiantes del Bachiller en Turismo confeccionan stands para presentar el Día del Turismo.</p>
					<div class="row">
						<div class="col-sm-6 col-md-4">
							<a class="lightbox" href="images/img-bachiller-turismo/dia-del-turismo-stand.jpg">
								<img src="images/img-bachiller-turismo/dia-del-turismo-stand.jpg" alt="Stand confeccionado por los Estudiantes de 11º Turismo">
							</a>
						</div>
						<div class="col-sm-6 col-md-4">
							<a class="lightbox" href="images/img-bachiller-turismo/dia-del-turismo-4.jpg">
								<img src="images/img-bachiller-turismo/dia-del-turismo-4.jpg" alt="Stand Elaborado por Estudiantes de X º A">
							</a>
						</div>
						<div class="col-sm-12 col-md-4">
							<a class="lightbox" href="images/img-bachiller-turismo/dia-del-turismo-stand-india.jpg">
								<img src="images/img-bachiller-turismo/dia-del-turismo-stand-india.jpg" alt="Stand confeccionado por X º B, representando al País de India">
							</a>
						</div>
					</div>
				</div>
			</div>
			<h3 class="pt-5 text-center">Día Ecológico</h3>
			<p class="text-center">Se organiza el día ecológico para concientizar a los estudiantes sobre la importancia de cuidar y proteger el ambiente.</p>
			<div class="row">
				<div class="galeria-imagenes">
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<a class="lightbox" href="images/img-bachiller-turismo/dia-ecologico.jpg">
								<img src="images/img-bachiller-turismo/dia-ecologico.jpg" >
							</a>
						</div>
						<div class="col-sm-6 col-md-6">
							<a class="lightbox" href="images/img-bachiller-turismo/dia-ecologico-2.jpg">
								<img src="images/img-bachiller-turismo/dia-ecologico-2.jpg" >
							</a>
						</div>
					</div>
				</div>
			</div>
			<h3 class="pt-5 text-center">Práctica Profesional</h3>
			<p class="text-center">Los estudiantes tienen la oportunidad de ir a una práctica profesional que le permite aplicar los conocimientos adquiridos en su formación.</p>
			<div class="row">
				<div class="galeria-imagenes">
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<a class="lightbox" href="images/img-bachiller-turismo/practica-profesional-2.jpg">
								<img src="images/img-bachiller-turismo/practica-profesional-2.jpg" alt="practica profesional grupo">
							</a>
						</div>
						<div class="col-sm-6 col-md-6">
							<a class="lightbox" href="images/img-bachiller-turismo/practica-profesional.jpg">
								<img src="images/img-bachiller-turismo/practica-profesional.jpg" alt="practica profesional">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<!-- Inicio footer-->
		<div class="row footer">
			<?php include ("sections/footer.html");?>
		</div>
	</div>
	<script src="js/jquery.js"></script>
	<script src="lib/parallax.js-1.5.0/parallax.min.js"></script>
	<script src="lib/bootstrap/js/bootstrap.min.js"></script>
	<script src="lib/baguetteBox/js/baguetteBox.min.js"></script>
	<script>baguetteBox.run('.galeria-imagenes', {
	captions: function(element) {
	return element.getElementsByTagName('img')[0].alt;
	}
	});</script>
</body>
</html>