
    <?php

session_start();

if (!isset($_SESSION["usuario"])){

header("Location:http://localhost/website_carmencontelombardo/inicio_sesion.php");
}
?><!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../lib/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../styles/index_css.css">
    
    <link rel="stylesheet" href="../lib/fontawesome/css/all.css">
    <link rel="stylesheet" href="../styles/all.css">
    <link rel="stylesheet" href="../styles/admin.css">
    <link rel="stylesheet" type="text/css" href="../Formulario_Codigo/Codigos.css">
    <!----------------js de control del menu------------->
    <script src="js/admin.js"></script>
    <title>Administración</title>
</head>

<body>


    <?php
/*
session_start();
session_destroy();
if (!isset($_SESSION["usuario"])){

header("location:inicio_sesion.php");
}*/ 
?>


        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">


                <div class="nav-side-menu">
                    <!-------------------Inicio lateral--------------->
                    <div class="brand">MENÚ</div>

                    <img src="../images/logo-instituto-original-01_opt.png" alt="" class="perfil_foto img-fluid">


                    <!---------cambiar a logo----->
                    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

                    <div class="menu-list">

                        <ul id="menu-content" class="menu-content collapse out">
                            <div class="brand"><i>Instituto Carmen Conte Lombardo</i></div>



                            <li class="collapsed">
                                <a href="index.php"><i class="fa fa-search fa-lg"></i>Estudiantes </a>
                            </li>




                            <li data-toggle="collapse" data-target="#service" class="collapsed">
                                <a href="../FormNoticias.php"><i class="fa fa-globe fa-lg"></i> Agregar nuevas noticias </a>
                            </li>


                            <li>
                                <a href="codigo.php"><i class="fa fa-key fa-lg"></i> Generar código </a>
                            </li>


                            <li>
                                <a href="administradores.php">
                                    <i class="fa fa-user-edit fa-lg"></i> Agregar administrador
                                </a>
                            </li>

                            <li>
                                <a href="salir.php">
                                    <i class="fa fa-power-off fa-lg"></i> Salir
                                </a>
                            </li>

                        </ul>
                    </div>

                </div>
                <!-------------------fin menú lateral--------------->

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">

              
                <br>

                <div class="contenido" id="divestud" style="display:hide">
                    <?php include('../Formulario_Codigo/cod2.php'); ?>
                </div>





            </div>
        </div>













        <script src="../js/jquery.js"></script>
        <script src="../lib/bootstrap/js/bootstrap.min.js"></script>
</body>
<!--g7--->

</html>