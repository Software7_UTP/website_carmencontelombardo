<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="lib/fontawesome/css/all.css">
		<link rel="stylesheet" href="styles/all.css">
		<link rel="stylesheet" href="styles/pastoral_juvenil.css"><!--realiza llamdo a la hoja de estilo propia de la pagina Pastoral-Juvenil--------->
		<title>Pastoral-Juvenil</title>
	</head>
	<body>
	
	<div class="container-fluid">
        <div class="row">
          <?php include "sections/menu.html"?>
        </div>
        <!-- Start Contacto-->
</div>
       <!--------efecto parallax con titulo de la pagina--------------->
<div class="parallax" data-parallax="scroll" data-image-src="images/bg-titles-page.png">
			<h1 class="parallax-title text-center py-5 text-shadow"><b>PASTORAL JUVENIL</b></h1>
		</div>

<div class="container"><!-----------llama la pagina con contenido------------->
     <?php include ("sections/p-y-p-contenido_pastoral_juvenil.html");?>
</div>

        	<div class="container-fluid">
        <div class="row footer">
          <?php include ("sections/footer.html");?>
        </div>
      </div>	


<script src="js/jquery.js"></script>
	<script src="lib/parallax.js-1.5.0/parallax.min.js"></script>
	<script src="lib/bootstrap/js/bootstrap.min.js"></script>

	
</body>
</html>