<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS y librerías que dan movimiento a las imágenes-->
   <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
    
    <link rel="stylesheet" href="lib/baguetteBox/css/baguetteBox.min.css">
    <link rel="stylesheet" href="images/style-gallery.css">
    <link rel="stylesheet" href="lib/fontawesome/css/all.css">
    <link rel="stylesheet" href="styles/all.css">
    <link rel="stylesheet" href="styles/proyectoeducativo_manual_convivencia.css">

    <title>Septiembre 8</title>
  </head>
  <body>

    <div class="container-fluid">
      <div class="row">
        <?php include("sections/menu.html");?>
      </div>
    </div>
    <!-- Titulo de la sección 8 de septiembre-->
    <div class="parallax" data-parallax="scroll" data-image-src="images/bg-titles-page.png">
      <h1 class="parallax-title text-center py-5 text-shadow"><b>8 DE SEPTIEMBRE</b></h1>
    </div><!-- galeria de la sección 8 de septiembre-->
   <section class="gallery-block galeria-imagenes">
   	<div class="container-fluid">
   		<div class="row">
   			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
   				<div class="card border-0 transform-on-hover">
   					<a class="lightbox" href="images/fechas-memorables/maria-diciembre-8.jpg"><img src="images/fechas-memorables/maria-diciembre-8.jpg" class="card-img-top"></a>
   					<div class="card-body">
   						<p class="card-text">La niña María, por su disponibilidad sin reserva especialmente en su Presentación es para todos los jóvenes modelo de escucha, entrega, Inocencia, transparencia y fidelidad.</p>
   					</div>
   				</div>
   			</div>
   			
   			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
   				<div class="card border-0 transform-on-hover">
   					<a class="lightbox" href="images/fechas-memorables/maria-diciembre8-2.jpg"><img src="images/fechas-memorables/maria-diciembre8-2.jpg" class="card-img-top"></a>
   					<div class="card-body">
   						<p class="card-text">Ella es la patrona de nuestro colegio y su fiesta la celebramos el 8 de septiembre, en nuestra institución.</p>
   					</div>
   				</div>
   			</div>
   		</div>
   	  <!-- Comienza sección de accesos directos-->
       <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <div class="card border-0 transform-on-hover">
            <a class="" href="aniversario.php"><img src="images/fechas-memorables/aniversario-carmen-conte-2.jpg" class="card-img-top"></a>
            <div class="card-body">
              <h4>Aniversario</h4>
              <a href="aniversario.php" class="btn btn-info">Ver más</a>
            </div>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <div class="card border-0 transform-on-hover">
            <a href="entrega_simbolos.php"><img src="images/fechas-memorables/entrega-de-simbolos.jpg" class="card-img-top"></a>
            <div class="card-body">
              <h4>Entrega de Símbolos</h4>
              <a href="entrega_simbolos.php" class="btn btn-info">Ver más</a>
            </div>
          </div>
        </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <div class="card border-0 transform-on-hover">
            <a class="" href="retiro_graduandos.php"><img src="images/fechas-memorables/retiro-graduandos.png" class="card-img-top"></a>
            <div class="card-body">
              <h4>Retiro de Graduandos</h4>
              <a href="retiro_graduandos.php" class="btn btn-info">Ver más</a>
            </div>
          </div>
        </div>
      </div><!--termina sección de accesos directos -->
   	</div>
   </section>

     <div class="container-fluid">
    <!-- Inicio footer-->
    <div class="row footer">
      <?php include ("sections/footer.html");?>
    </div>
  </div>
  <script src="js/jquery.js"></script>
  <script src="lib/parallax.js-1.5.0/parallax.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/baguetteBox/js/baguetteBox.min.js"></script>
  <script>baguetteBox.run('.galeria-imagenes', {
  captions: function(element) {
  return element.getElementsByTagName('img')[0].alt;
  }
  });</script>
  </body>
</html>










