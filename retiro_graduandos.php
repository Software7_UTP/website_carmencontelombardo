<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="lib/baguetteBox/css/baguetteBox.min.css">
    <link rel="stylesheet" href="lib/fontawesome/css/all.css">
    <link rel="stylesheet" href="styles/all.css">
    <!-- título de la página-->
    <title>Retiro de Graduandos</title>
  </head>
  <body>
    <!-- Barra de navegación-->
    <div class="container-fluid">
      <div class="row">
        <?php include("sections/menu.html");?>
      </div>
    </div>
    <!-- Titulo-->
    <div class="parallax" data-parallax="scroll" data-image-src="images/bg-titles-page.png">
      <h1 class="parallax-title text-center py-5 text-shadow"><b>RETIRO DE GRADUANDOS</b></h1>
    </div>
    <!-- Contenido de retiro de graduandos-->
    <div class="container-fluid bg-pr">
      <div class="container">
        <div class="py-5 text-gray">
          <h2>¿Qué es el Retiro Espiritual?</h2>
          <h2>Ex. 24, 1-18</h2>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <p>El retiro es considerado como un tiempo de gracia en el que los jóvenes suelen experimentar emociones encontradas, por una parte la alegría de ver realizados sus sueños de ser Bachilleres y por otro lado la tristeza de la inminente separación de sus compañeros y amigos que por varios años han caminado, compartido y crecido juntos.</p>
            <div class="galeria-imagenes">
              <a class="lightbox img-fluid" href="images/fechas-memorables/padres-de-famlilia-carmen-conte.png">
                <img src="images/fechas-memorables/padres-de-famlilia-carmen-conte.png " >
              </a>
            </div>
            
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="container ">
              <p>Como punto de referencia se toma la experiencia vivida por Moisés en su encuentro con Yavé:</p>
              <ul class="list-group">
                <li class="list-group-item list-group-item-success">Moisés no sube sólo, va con sus más allegados, su hermano</li>
                <li class="list-group-item list-group-item-secondary">En ese lugar hace un pacto con el Señor</li>
                <li class="list-group-item list-group-item-info">Moisés reconoce sus raíces, su origen, su misión</li>
                <li class="list-group-item list-group-item-warning">Moisés Subió, escuché, descendió y volvió a la vida regular con una norma de vida para Él y para su pueblo; esto le permite tener criterios, bases sólidas y fundamento para cimentar su vida</li>
                <li class="list-group-item list-group-item-light text-dark">También el pueblo experimentará el plan de Dios que le reserva para el futuro</li>
                <li class="list-group-item list-group-item-primary">La vida de Moisés nunca jamás volvió a ser la misma, su encuentro con el Dios verdadero, cambió su existencia</li>
                <li class="list-group-item list-group-item-success">Al bajar de aquí llevaremos lo que cada uno quiera llevar de acuerdo al espacio que ha hecho en su vida para Dios. </li>
                
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- Inicia container de galería-->
      <div class="container">
        <div class="row">
          <div class="galeria-imagenes">
            <div class="row">
              <div class="col-sm-12 col-md-6">
                <a class="lightbox" href="images/fechas-memorables/estudiantes-carmen-conte-lombardo-1.png">
                  <img src="images/fechas-memorables/estudiantes-carmen-conte-lombardo-1.png" >
                </a>
              </div>
              <div class="col-sm-6 col-md-6">
                <a class="lightbox" href="images/fechas-memorables/estudiantes-carmen-conte-lombardo.png">
                  <img src="images/fechas-memorables/estudiantes-carmen-conte-lombardo.png">
                </a>
              </div>
              
            </div>
          </div>
        </div>
      </div>
      <!-- Termina container-->
    </div>
    <!-- Inicia container-->
    <div class="container">
      <div class="row fondo_seccion_primary"><!-- inicia fila-->
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              
              <p>En un Retiro Espiritual, la persona tiene la oportunidad de tomar distancia de la vida diaria, permanecer en silencio, para escuchar a Dios, descubrir el sentido de lo que está viviendo, reconocer las fuerzas de Vida y de Muerte en sí misma y en lo que la rodea, tomar decisiones en libertad, ordenar su vida, según los deseos de Dios, para más AMAR y SERVIR a sus hermanos. </p>
              
            </div>
          </div>
        </div>
      </div><!-- termina fila-->
      <div class="row py-4"><!--comienza fila -->
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
          <div class="container-fluid">
            <p> Un retiro espiritual es un tiempo para meditar profundamente acerca de Dios y nuestra respuesta a su amor. </p>
            <p>Hay que aprender a meditar, ejm. confrontar lo escuchado o leído con nuestra propia vida.</p>
          </div>
          
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
          <div class="container-fluid">
            <p>Es un tiempo para descubrir quiénes somos y qué somos; para lograr esta tarea es conveniente "retirarse" incluso geográficamente, Por eso, existen "casas de retiro". Lo importante es hacerlo sin apuro, sin querer hacer mil cosas, ni leer varios libros.</p>
            
          </div>
          
        </div>
      </div><!-- termina fila -->
      <!-- incia fila-->
      <div class="row fondo_seccion_primary">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
              <p>El retiro se puede hacer solo o en grupo. Para alguien sin experiencia en esto es mejor hacerlo grupalmente. En el retiro, Cristo nos habla y nos toca estar atentos a sus palabras y entregarnos a su amor. Al contemplar a Cristo, se nos devela el misterio trinitario y aprendemos como mejorar nuestra relación con los demás. Para hacer un buen retiro necesitamos: </p>
              
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <ul class="list-group">
                <li class="list-group-item list-group-item-success">Tener espacios de reflexión personal: Tiempo solo para ti, para hablar contigo y con Dios</li>
                <li class="list-group-item list-group-item-danger">Ambiente de total silencio (en el comedor, en el dormitorio, en todo momento)</li>
                <li class="list-group-item list-group-item-info">Lo más alejado que puedan estar de quienes creen les puedan impedir estar en retiro</li>
                <li class="list-group-item list-group-item-warning">En los dormitorios sólo se escuchará el ruido propio de la naturaleza, el canto de las aves</li>
                <li class="list-group-item list-group-item-light text-dark">Seguir las instrucciones dada en cada momento por el guía, a ejm de Moisés</li>
                <li class="list-group-item list-group-item-primary">Olvidarnos de los celulares durante el tiempo de retiro.</li>
                
              </ul>
            </div>
          </div>
        </div>
      </div><!-- termina fila-->
      <br>
      <!-- incia container de graduación -->
      <div class="container text-gray">
        <h2>Graduación</h2>
        <p>El momento más solemne para un estudiante: Recibir el diploma que lo acredita  como bachiller de una determinada especialidad, en un colegio, que seguirá siendo su alma materna.</p>
        <div class="row">
          <div class="galeria-imagenes">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <a class="lightbox" href="images/fechas-memorables/graduandos.jpg">
                  <img src="images/fechas-memorables/graduandos.jpg">
                </a>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <a class="lightbox" href="images/fechas-memorables/tarima-carmen-conte-lombardo.jpg">
                  <img src="images/fechas-memorables/tarima-carmen-conte-lombardo.jpg" >
                </a>
              </div>
            </div>
          </div>
        </div>
      </div><!-- termina container de graduación-->
      <!-- inicia fila de accesos directos-->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <div class="card border-0 transform-on-hover">
            <a class="" href="aniversario.php"><img src="images/fechas-memorables/aniversario-carmen-conte-lombardo.jpg" class="card-img-top"></a>
            <div class="card-body">
              <h4>Aniversario</h4>
             <a href="aniversario.php" class="btn btn-info">Ver más</a>
            </div>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <div class="card border-0 transform-on-hover">
            <a class="" href="septiembre8.php"><img src="images/fechas-memorables/maria-diciembre-8.jpg" class="card-img-top"></a>
            <div class="card-body">
              <h4>8 de Septiembre</h4>
              <a href="septiembre8.php" class="btn btn-info">Ver más</a>
            </div>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <div class="card border-0 transform-on-hover">
            <a href="entrega_simbolos.php"><img src="images/fechas-memorables/entrega-de-simbolos-2.png" class="card-img-top"></a>
            <div class="card-body">
              <h4>Entrega de Símbolos</h4>
              <a href="entrega_simbolos.php" class="btn btn-info">Ver más</a>
            </div>
          </div>
        </div>
      </div>
      <!-- termina fila de accesos directos -->
    </div>
    <div class="container-fluid">
      <!-- Inicio footer-->
      <div class="row footer">
        <?php include ("sections/footer.html");?>
      </div>
    </div><!-- fin del footer-->
    <!-- Librerías que le dan movimiento a las imágenes -->
    <script src="js/jquery.js"></script>
    <script src="lib/parallax.js-1.5.0/parallax.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="lib/baguetteBox/js/baguetteBox.min.js"></script>
    <script>baguetteBox.run('.galeria-imagenes', {
    captions: function(element) {
    return element.getElementsByTagName('img')[0].alt;
    }
    });</script>
  </body>
</html>