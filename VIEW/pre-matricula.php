<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Pre-Matricula</title>
		<link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" >
		<link href="../estilo-pre-matricula.css" rel="stylesheet" type="text/css" >
		
	</head>
	<body>
		<div class="container">
			<div class="row">				
				<form action="../MODEL/Datos.php" method="POST" role="form">
					<h2>FORMULARIO DE INSCRIPCIÓN PRIMER INGRESO - GENERALES DEL ESTUDIANTE</h2>
					<div class="form-group">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<fieldset>
						<legend for="">DATOS DEL ESTUDIANTE</legend><br>

						<label for="">Primer Nombre(*)</label><br>
						<input type="text" class="form-control" values="'.nombre_1.'" name="nombre_1" placeholder="" required="required" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+"><br><br>

						<label for="">Segundo Nombre</label><br>
						<input type="text" class="form-control" values="'.nombre_2.'" name="nombre_2" placeholder="" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+"><br><br>

						<label for="">Apellido Paterno(*)</label><br>
						<input type="text" class="form-control" values="'.apellido_pa.'" name="apellido_pa" placeholder="" required="required" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+"><br><br>

						<label for="">Apellido Materno(*)</label><br>
						<input type="text" class="form-control" values="'.apellido_ma.'" name="apellido_ma" placeholder="" required="required" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+"><br><br>

						<label for="">Cédula/Pasaporte</label><br>
						<input class="form-control" type="text" id="" values="'.cedula_est.'" name="cedula_pro" required="required" pattern="{1}[0-9]{1,4}[-]{1}[0-9]{1,4}[-]{1}[0-9]{1,4}"><br>

						<br><label for="" required="required">Sexo</label><br>
						<select id="" values="'.sexo.'" name="sexo" required="required" class="form-control">
							<option selected value="">Elije una opción</option>
							<option value="M">Masculino</option>
							<option value="F">Femenino</option>
						</select>
							<br><br>

						<label for="">Fecha de Nacimiento (dd/mm/aaaa) (*)</label><br>
						<input type="date" class="form-control" values="'.fecha_naci.'" name="nombre_pro" placeholder="01/01/2001" required="required"><br><br>

						</fieldset>
						<!---->
						<fieldset>
						<legend for="">DIRECCIÓN RESIDENCIAL Y CONTACTOS</legend><br>

						<label for="">Provincia</label><br>
						<input type="text" class="form-control" values="'.provincia_est.'" name="provincia_est" placeholder="" required="required" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+"><br><br>

						<label for="">Distrito</label><br>
						<input type="text" class="form-control" values="'.distrito_est.'" name="distrito_est" placeholder="" required="required" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+"><br><br>

						<label for="">Corregimiento</label><br>
						<input type="text" class="form-control" values="'.corregi_est.'" name="corregi_est" placeholder="" required="required" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+"><br><br>

						<label for="">Comunidad</label><br>
						<input type="text" class="form-control" values="'.comunidad_est.'" name="comunidad_est" placeholder="" required="required" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+"><br><br>

						<label for="">Teléfono/Cedular</label><br>
						<input type="text" class="form-control" values="'.telefono_est.'" name="telefono_est" placeholder="6123-1234" required="required" pattern="[6]{1}[0-9]{3}[-]{1}[0-9]{4}"><br><br>

						</fieldset>

						<!---->

						<fieldset>
						<legend for="">PLANTEL DE PROCEDENCIA</legend><br>

						<label for="">Centro Educativo</label><br>
						<input type="text" class="form-control" values="'.centro_edu.'" name="centro_edu" placeholder="" required="required" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+"><br><br>

						<label for="">Distrito</label><br>
						<input type="text" class="form-control" values="'.distrito_plant.'" name="distrito_plant" placeholder="" required="required" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+"><br><br>

						<label for="">Corregimiento</label><br>
						<input type="text" class="form-control" values="'.corregi_plant.'" name="corregi_plant" placeholder="" required="required" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+"><br><br>

						<label for="">Comunidad</label><br>
						<input type="text" class="form-control" values="'.comunidad_plant.'" name="comunidad_plant" placeholder="" required="required" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+"><br><br>

						</fieldset>


						
						


						<!--funcion fin-->
						<fieldset>
						<legend for="">INSCRIPCIÓN PARA EL PERIODO ESCOLAR</legend>
						<label for="">Oferta Académica</label>
                   	<div class="radio">
                   		<label>
                   			<input type="radio" name="oferta_a" id="input" value="General" required="required" onClick="cambio(this.value);">
                   			Básica General
                   		</label>
                   		<label>
                   			<input type="radio" name="oferta_a" id="input" value="Ciencias" required="required" onClick="cambio(this.value);">
                   			Bachiller en Ciencias
                   		</label>
                   		<label>
                   			<input type="radio" name="oferta_a" id="input" value="Turismo" required="required"onClick="cambio(this.value);">
                   			Bachiller en Turismo
                   		</label>
                   	</div>

                   	<label for="">Seleccionar Nivel - Básica General</label>
                   	<div class="radio">
                   		<label>
                   			<input type="radio" name="grado_secu" id="element_58_1" value="7" required="required" disabled>
                   			7°
                   		</label>
                   		<label>
                   			<input type="radio" name="grado_secu" id="element_58_2" value="8" required="required" disabled>
                   			8°
                   		</label>
                   		<label>
                   			<input type="radio" name="grado_secu" id="element_58_3" value="9" required="required" disabled>
                   			9°
                   		</label>
                   	</div>

                   	<label for="">Seleccionar Nivel - Bachilleres</label>
                   	<div class="radio">
                   		<label>
                   			<input type="radio" name="grado_secu" id="element_57_1" value="10" required="required" disabled>
                   			10°
                   		</label>
                   		<label>
                   			<input type="radio" name="grado_secu" id="element_57_2" value="11" required="required" disabled>
                   			11°
                   		</label>
                   		<label>
                   			<input type="radio" name="grado_secu" id="element_57_3" value="12" required="required" disabled>
                   			12°
                   		</label>
                   	</div>

                   	<label for="">Tipo de Estudiante</label>
                   	<div class="radio">
                   		<label>
                   			<input type="radio" name="tipo_est" id="input" value="Regular" required="required">
                   			Regular
                   		</label>
                   		<label>
                   			<input type="radio" name="tipo_est" id="input" value="Reprobado" required="required">
                   			Reprobado
                   		</label>
                   	</div><br>
                   		<!--FIN METER FUNCION DE CAMBIAR-->
                   			<script language='javascript'> 
							function cambio(X){
							  if(X == 'General'){
							    $("#element_57_1").attr('disabled','disabled');
							    $("#element_57_1").attr('checked', false);
							    $("#element_57_2").attr('disabled','disabled');
							    $("#element_57_2").attr('checked', false);
							    $("#element_57_3").attr('disabled','disabled');
							    $("#element_57_3").attr('checked', false);

							    $("#element_58_1").removeAttr('disabled');
							    $("#element_58_2").removeAttr('disabled');
							    $("#element_58_3").removeAttr('disabled');

							  }else if(X == 'Ciencias' || X == 'Turismo'){
							    $("#element_57_1").removeAttr('disabled');
							    $("#element_57_2").removeAttr('disabled');
							    $("#element_57_3").removeAttr('disabled'); 

							    $("#element_58_1").attr('disabled','disabled');
							    $("#element_58_1").attr('checked', false);
							    $("#element_58_2").attr('disabled','disabled');
							    $("#element_58_2").attr('checked', false);
							    $("#element_58_3").attr('disabled','disabled');
							    $("#element_58_3").attr('checked', false);
							  }
							}

							 </script>
                   		<!---->
						</fieldset>

					<fieldset>
						<legend>EL ESTUDIANTE RECIBE LOS SIGUIENTES BENEFICIOS</legend>

						<label for="">Beca de Estudio</label>
                   	<select name="beca" id="" values="'.beca.'" required="required" class="form-control">
                   		<option  selected value="">Elije una opción</option>
                   		<option value="Universal">Universal</option>
                   		<option value="Concurso">Concurso</option>
                   		<option value="Honorifica">Honorifica</option>
                   		<option value="Ninguna">Ninguna</option>
                   	</select><br>

                   	<label for="">Apoyo Social</label>
                   	<select name="apoyo_so" id="" values="'.apoyo_so.'" required="required" class="form-control">
                   		<option  selected value="">Elije una opción</option>
                   		<option value="Angel Guardian">Ángel Guardián</option>
                   		<option value="Red de Oportunidades">Red de Oportunidades</option>
                   		<option value="120 a los 65">120 a los 65</option>
                   		<option value="Ninguna">Ninguna</option>
                   	</select><br>


                   	<label for="">¿Posee Alguna Discapacidad?</label>
                   	<div class="radio">
                   		<label>
                   			<input type="radio" name="discapacidad" id="input" value="Si" required="required" onClick="cambiodis(this.value);">
                   			Sí 
                   		</label>
                   		<label>
                   			<input type="radio" name="discapacidad" id="input" value="No" required="required" onClick="cambiodis(this.value);">
                   			No
                   		</label><br><br>
                   		<select name="tipo_disca" id="element_70_1" values="'.disca.'" required="required" class="form-control" disabled>
                   		<option  selected value="" >Elije una opción</option>
                   		<option value="Lento Aprendizaje" disable>Lento Aprendizaje</option>
                   		<option value="Auditivo">Auditivo</option>
                   		<option value="Visual">Visual</option>
                   		<option value="Fisico">Físico</option>
                   	</select><br>
                   	</div><br>

                   	<!--funciuon de habilitar la opcion de discapacidad-->
                   			<script language='javascript'> 
							function cambiodis(X){
							  if(X == 'Si'){
							 	$("#element_70_1").removeAttr('disabled');
							  }else{
							    $("#element_70_1").attr('disabled','disabled');
							    $("#element_70_1").attr('checked', false);
							  }
							} </script>
					<!---->
					</fieldset>


					<fieldset>
						<legend>EL ESTUDIANTE POSEE HABILIDADES O PRÁCTICA</legend>
						<label for="">¿Práctica alguna disciplina?</label>
						<div class="radio">
                   		<label>
                   			<input type="radio" name="disciplina" id="input" value="Si" required="required" onClick="cambiodepor(this.value);">
                   			Sí 
                   		</label>
                   		<label>
                   			<input type="radio" name="disciplina" id="input" value="No" required="required" onClick="cambiodepor(this.value);">
                   			No
                   		</label><br><br>
                   		<p>Especifique(*)</p>
                   		<input type="text" name="tipo_disciplina" id="element_80_1" class="form-control" value="" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="" disabled>
                   	</div><br>
					<!--funcion para habilitar la opcion de agregar disciplina-->
                   			<script language='javascript'> 
							function cambiodepor(X){
							  if(X == 'Si'){
							 	$("#element_80_1").removeAttr('disabled');
							  }else{
							    $("#element_80_1").attr('disabled','disabled');
							    $("#element_80_1").attr('checked', false);
							    
							    
							  }
							} </script>
					<!---->

					</fieldset>

					<fieldset>
						<legend>DATOS DEL ACUDIENTE</legend>
							<label for="">Nombre Completo(*)</label><br>
						<input type="text" class="form-control" values="'.nombre_acu.'" name="nombre_acu" placeholder="" required="required" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+"><br>

						<label for="">Cédula(*)</label><br>
						<input type="text" class="form-control" values="'.cedula_acu.'" name="cedula_acu" placeholder="" required="required" pattern="{1}[0-9]{1,4}[-]{1}[0-9]{1,4}[-]{1}[0-9]{1,4}" ><br>

						<label for="">Teléfono(*)</label><br>
						<input type="text" class="form-control" values="'.tel_acu.'" name="tel_acu" placeholder="" required="required" pattern="{1}[0-9]{1,4}[-]{1}[0-9]{1,4}[-]{1}[0-9]{1,4}"><br>

						<label for="">Correo Electrónico(*)</label><br>
						<input type="email" class="form-control" values="'.correo_acu.'" name="correo_acu" placeholder="" required="required"><br><br>
					</fieldset>

					<button type="submit" name="enviar" class="btn btn-primary">ENVIAR DATOS</button><br><br>
					<!--fin columna 1-->
						</div>	

						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<!--inicio columna 2-->


						</div>
						</div>	


						<?php
                    /*
							INICIALIZACIÓN DE LAS VARIABLES DEL FORMULARIO
                    */
					 if(isset($_POST['enviar'])){
					 	//fielset "DATOS DEL ESTUDIANTE"
					@$nombre_1=$_POST["nombre_1"];
					@$nombre_2=$_POST["nombre_2"];
					@$apellido_pa=$_POST["apellido_pa"];
					@$apellido_ma=$_POST["apellido_ma"];
					@$cedula_est=$_POST["cedula_est"];
					@$sexo=$_POST["sexo"];
					@$fecha_naci=$_POST["fecha_naci"];
					
					//fielset "DIRECCIÓN RESIDENCIAL Y CONTACTOS"

					@$provincia_est=$_POST["provincia_est"];
					@$distrito_est=$_POST["distrito_est"];
					@$corregi_est=$_POST["corregi_est"];
					@$comunidad_est=$_POST["comunidad_est"];
					@$telefono_est=$_POST["telefono_est"];

					//fielset "PLANTEL DE PROCEDENCIA"

					@$centro_edu=$_POST["centro_edu"];
					@$distrito_plant=$_POST["distrito_plant"];
					@$corregi_plant=$_POST["corregi_plant"];
					@$comunidad_plant=$_POST["comunidad_plant"];

					//fielset "INSCRIPCIÓN PARA EL PERIODO ESCOLAR"

					@$oferta_a=$_POST["oferta_a"];
					@$grado_secu=$_POST["grado_secu"];
					@$tipo_est=$_POST["tipo_est"];

					//fielset "EL ESTUDIANTE RECIBE LOS SIGUIENTES BENEFICIOS"

					@$beca=$_POST["beca"];
					@$apoyo_so=$_POST["apoyo_so"];
					@$discapacidad=$_POST["discapacidad"];
					@$tipo_disca=$_POST["tipo_disca"];

					//fielset "EL ESTUDIANTE POSEE HABILIDADES O PRÁCTICA"

					@$disciplina=$_POST["disciplina"];
					@$tipo_disciplina=$_POST["tipo_disciplina"];

					//fielset "DATOS DEL ACUDIENTE"

					@$nombre_acu=$_POST["nombre_acu"];
					@$cedula_acu=$_POST["cedula_acu"];
					@$tel_acu=$_POST["tel_acu"];
					@$correo_acu=$_POST["correo_acu"];
					
				
					
                   	//CREACIÓN DEL OBJETO QUE ENVIA LOS DATOS.

                   		//CREACIÓN DEL OBJETO QUE ENVIA LOS DATOS.
  					//concatenacion del tomo con la cedula para crearla correctamente y no salga separada
		 			 $inserta= new Funcionesbd($nombre_1, $nombre_2, $apellido_pa, $apellido_ma, $cedula_est, $sexo, $fecha_naci, $provincia_est, $distrito_est, $corregi_est, $comunidad_est,
		 			 $telefono_est,$centro_edu, $distrito_plant, $corregi_plant,$comunidad_plant, $oferta_a, $grado_secu, $tipo_est, $beca, $apoyo_so, $discapacidad, $tipo_disca, $disciplina,
		 			 $tipo_disciplina, $nombre_acu, $cedula_acu, $tel_acu, $correo_acu);
		 			 $inserta->insertar();
					
		                   	
  					}//llave del isset

  ?>

					</form>
				</div>
			</div>

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
 		<script src="Hello World"></script>
	</body>
</html>