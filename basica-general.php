<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="lib/fontawesome/css/all.css">
		<link rel="stylesheet" href="styles/all.css">
		<link rel="stylesheet" href="styles/basica_general_css.css">
		<title>Básica General</title>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<?php include ("sections/menu.html");?>
			</div>
		</div>
		<!-- Titulo-->
		<div class="parallax" data-parallax="scroll" data-image-src="images/bg-titles-page.png">
			<h1 class="parallax-title text-center py-5 text-shadow"><b>BÁSICA GENERAL</b></h1>
		</div>
		<!-- Contenido -->
		<div class="container-fluid py-4 bg-pr">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 pl-0 pr-0 ">
						<img class="img-fluid" src="images/img-basicageneral/basica-general-header.jpg" alt="educación en valores">
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
						<h2 class=" text-section-title text-gray" >La enseñanza Religiosa Escolar y la Educación en Valores Constituyen pilares fundamentales en nuestra educación, como escuela católica.</h2>
						<a class="btn btn-danger text-center" href="sections/plan-de-estudio-basica-general.pdf" target="_blank">Ver plan de estudio</a>
					</div>
				</div>
			</div>
		</div>
		<!-- Descripción de básica general -->
		<div class="container-fluid">
			<div class="row fondo_seccion_primary">
				<div class="container text-gray">
					<p>Como Institución educativa que educa en valores, se asume el desafío de educar teniendo como mira y prioridad  a los menores, para que juntos,  persona  e institución,  construyamos nuevos modelos educativos  en donde cada uno descubra  la importancia de su autoformación y autodeterminación que permita desarrollar las competencias necesarias en el campo profesional, resolviendo los problemas de la comunidad.</p>
					<p>El Programa de Valores de este centro educativo tiene como objetivo formar una persona íntegra, capaz de hacer frente al mundo que le rodea, capaz de aceptar y asumir la realidad tal como es,  capaz de saber ser un agente de cambio  que se vaya realizando y desarrollando como un ser en proyección. El Programa en Valores es una de las características más importantes del Instituto Carmen Conte Lombardo y es sustentable por los significativos reconocimientos que ostenta.</p>
				</div>
			</div>
		</div>
		<!--  Agropecuaria-->
		<div class="container-fluid bg-pr">
			<div class="container py-3 text-gray">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 ">
						<h2>Agropecuaria</h2>
						<p>La agropecuaria como materia tecnológica es muy importante en el  INSTITUTO CARMEN CONTE LOMBARDO; ella tiene como objetivo primordial implementar prácticas de agricultura para la subsistencia cotidiana en nuestro centro educativo y  en los hogares de nuestros estudiantes. </p>
						<p>La materia   de tecnología  agropecuaria en nuestro centro educativo nos permite  garantizar la enseñanza a nuestros estudiantes los cuales obtendrán conocimiento sobre la metodología, técnicas y uso de equipos  para la producción de alimentos en la institución para el bienestar y la salud de nuestros  alumnos.</p>
						<p>Desde el inicio de nuestro centro educativo, gracias a la agricultura, se ha tratado sobre todo, de implementar nuevas  técnicas de cultivo del suelo para la obtención controlada de vegetales. Con esto se encontró la solución para el abastecimiento de vegetales y producto pecuario para el comedor del  centro educativo.
							De este modo, gracias a la tecnología  se pudo mejorar los manejos de los cultivos agrícolas y el área pecuaria,  además se pudo mejorar la  enseñanza y aprendizaje  utilizando  metodología didáctica y práctica en nuestros alumnos para que la realicen en sus hogares.
						</p>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 m-auto">
						<img src="images/img-basicageneral/agropecuaria-2.jpg" class="img-fluid img-tamano1" alt="Estudiante con maquinaria agropecuaria">
						<div id="carouselExampleIndicators1" class="carousel slide my-5" data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
								<li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
							</ol>
							<div class="carousel-inner">
								<div class="carousel-item active">
									<img class="d-block w-100" src="images/img-basicageneral/agropecuaria.jpg" alt="estudiantes en cultivos">
								</div>
								<div class="carousel-item">
									<img class="d-block w-100" src="images/img-basicageneral/agropecuaria-3.jpg" alt="estudiantes en agricultura">
								</div>
							</div>
							<a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>						
					</div>
				</div>
			</div>
		</div>
		<!-- áreas de la materia de técnologia Comercio -->
		<!-- Tecnología  -->
		<div class="parallax" data-parallax="scroll" id="bg-tec" >
			<div class="container-fluid text-white">
				<div class="row ">
					<div class="col-xs-12 col-md-12 col-lg-6 p-0">
						<img src="images/img-basicageneral/investigacion-en-computadoras.jpg" class="img-fluid " alt="Estudiantes con computadores">
					</div>
					<div class="col-xs-12 col-md-12 col-lg-6 py-4">
						<h3 class="text-secondary-title text-center pb-4">La asignatura Tecnología Comercio se subdivide en tres áreas en el nivel Pre-Media</h3>
						<div class="container">
							<div class="row">
								<div class="align-to-center">
									<div class="icons-centrados">
										<i class="fas icono-contenedor fa-keyboard fa-3x" aria-hidden="true"></i>
										<p class="texto-contenedor">Mecanografía Computarizada</p>
									</div>
								</div>
							</div>
							<div class="row pt-2 pb-3">
								<div class="align-to-center">
									<div class="icons-centrados">
										<i class="fas fa-cash-register icono-contenedor fa-3x" aria-hidden="true"></i>
										<p class="texto-contenedor">Introducción al comercio</p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="align-to-center">
									<div class="icons-centrados">
										<i class="fas fa-handshake icono-contenedor fa-3x" aria-hidden="true"></i>
										<p class="texto-contenedor">Principios de contabilidad</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row fondo_seccion_primary">
				<div class="container text-gray">
					<h2>Tecnología</h2>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							
							<p>En el área de Aplicaciones Tecnológicas básicas se ofrece un programa de Mecanografía Computarizada, para el séptimo grado.  Aquí se enfatiza el aprendizaje del teclado guía y el uso adecuado de una computadora para la digitación de trabajos, informes u otros escritos.</p>
							<p>En cuanto al desarrollo comercial, para octavo grado son objeto de estudio y análisis los aspectos generales más importantes de la actividad económica de nuestro país; sistemas monetarios, sistemas bancarios, la empresa en el mundo de hoy, la micro, pequeña y mediana empresa.</p>
							<p>En el área de Contabilidad se brindan los conocimientos importantes para la adquisición de destrezas y actitudes en la actividad contable. Ella contribuye a la formación de futuros profesionales aptos para preparar estados financieros, analizar cuentas y organizar sus propias economías. Principios de Contabilidad se imparte en el noveno grado.</p>
							<div id="carouselExampleIndicators2" class="carousel slide my-5" data-ride="carousel">
								<ol class="carousel-indicators">
									<li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
									<li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
									<li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>								
								</ol>
								<div class="carousel-inner">
									<div class="carousel-item active">
										<img class="d-block w-100" src="images/img-basicageneral/tecnologia.jpg" alt="Estudiantes en clases de tecnología">
									</div>
									<div class="carousel-item">
										<img class="d-block w-100" src="images/img-basicageneral/discusion-de-investigacion.jpg" alt="discución de investigaciones">
									</div>
									<div class="carousel-item">
										<img class="d-block w-100" src="images/img-basicageneral/charlas.jpg" alt="charla de estudiantes">
									</div>
								</div>
								<a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
									<span class="carousel-control-prev-icon" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
								</a>
								<a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
									<span class="carousel-control-next-icon" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
								</a>
							</div>
							<p>En estas áreas correspondientes a Tecnología Comercio, se busca orientar al estudiante hacia el desarrollo de sus habilidades, destrezas y actitudes de tal manera que contribuyan a su formación como futuro profesional y ciudadano productivo del país.  Representa una ventana exploratoria de modo tal que el egresado de la etapa pre-media frente a las opciones del bachillerato pueda continuar la educación media, en función de sus vivencias, seleccionar el bachillerato con el cual se sienta más identificado.</p>
							
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ">
							<img src="images/img-basicageneral/investigacion-2.jpg" class="img-fluid my-3" alt="Estudiantes con computadores">
							
							<p>Estas áreas procuran fortalecer los conocimientos, habilidades y destrezas relativas al desarrollo y aplicación de la tecnología en la actualidad. Además propician la exploración y orientación vocacional para continuar estudios en el nivel medio; así como desarrollan competencias básicas para desarrollar trabajos sencillos.</p>
							<div class="card" >
								<img class="card-img-top" src="images/img-basicageneral/tecnologia-robotica.jpg" alt="robótica">
								<div class="card-body">
									<p class="card-text">Los estudiantes de pre-media también desarrollan sus habilidades en el área de la robótica.</p>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Familia y desarrollo -->
		<div class="container-fluid bg-pr mb-4">
			<div class="container text-gray">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 pt-3">
						<h2>Familia y Desarrollo</h2>
						<p>Familia y Desarrollo Comunitario, es una materia académica y formativa que se imparte en todas las escuelas públicas y privadas del país por su importancia en la formación integral del educando.
							En sus contenidos académicos los estudiantes adquieren conocimientos teóricos y prácticos significativos, que los prepara para hacerle frente a las diferentes situaciones y conflictos que se les pueda presentar en la vida, tomando decisiones asertivas que les permitan solucionar dichos desafíos, de tal manera que el resultado de esta formación los hará personas competitivas dentro de la sociedad.
							Todos estos conocimientos se imparten en las clases a través de sus cinco áreas de estudio como son: Alimentación y Nutrición, Textiles y Vestuario, Desarrollo humano y sexualidad, Administración del hogar, Vivienda y su ambiente.
							<p>La confección de artesanías regionales es parte importante en la formación que reciben los estudiantes de Premedia.</p>
						</p>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 my-auto">
						<div id="carouselExampleIndicators3" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#carouselExampleIndicators3" data-slide-to="0" class="active"></li>
								<li data-target="#carouselExampleIndicators3" data-slide-to="1"></li>
								<li data-target="#carouselExampleIndicators3" data-slide-to="2"></li>
							</ol>
							<div class="carousel-inner">
								<div class="carousel-item active">
									<img class="d-block w-100" src="images/img-basicageneral/confeccion-de-artesania-2.jpg" alt="niñas confeccionando artesanias">
								</div>
								<div class="carousel-item">
									<img class="d-block w-100" src="images/img-basicageneral/confeccion-de-artesania.jpg" alt="artesanias">
								</div>
								<div class="carousel-item">
									<img class="d-block w-100" src="images/img-basicageneral/familia-y-desarrollo.jpg" alt="familia y desarrollo">
								</div>
							</div>
							<a class="carousel-control-prev" href="#carouselExampleIndicators3" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#carouselExampleIndicators3" role="button" data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<!-- Inicio footer-->
		<div class="row footer">
			<?php include ("sections/footer.html");?>
		</div>
	</div>
	<script src="js/jquery.js"></script>
	<script src="lib/parallax.js-1.5.0/parallax.min.js"></script>
	<script src="lib/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>