<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS y librerías que dan movimiento a las imágenes-->
   <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
    
    <link rel="stylesheet" href="lib/baguetteBox/css/baguetteBox.min.css">
    <link rel="stylesheet" href="images/style-gallery.css">
    <link rel="stylesheet" href="lib/fontawesome/css/all.css">
    <link rel="stylesheet" href="styles/all.css">
    <link rel="stylesheet" href="styles/proyectoeducativo_manual_convivencia.css">

    <title>Nuestros Egresados Hablan</title>
  </head>
  <body>

    <div class="container-fluid">
      <div class="row">
        <?php include("sections/menu.html");?>
      </div>
    </div>
    <!-- Titulo de la sección nuestros egresados hablan-->
    <div class="parallax" data-parallax="scroll" data-image-src="images/bg-titles-page.png">
      <h1 class="parallax-title text-center py-5 text-shadow"><b>NUESTROS EGRESADOS HABLAN</b></h1>
    </div><!-- galeria de la sección nuestros egresados hablan-->
   <section class="gallery-block galeria-imagenes bg-pr">
   	<div class="container-fluid">
   		<div class="row"><!-- inicia fila de primera aportacion de egresados-->
   			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
   				
   					
   						<p class="text-black">Formar parte de la Familia Lombardina, consideramos que fue una gran experiencia nuestros años de estudios en el Instituto Carmen Conte Lombardo, ya que nos sentíamos como en casa; los compañeros de bachiller nos apoyábamos y nuestros docentes nos motivaban constantemente a superarnos y practicar los valores (detalle que en otras escuelas no experimentamos).
</p>
<p class="text-black">Como parte del Bachiller en Turismo, estudiábamos, practicábamos los conceptos teóricos llevados a la realidad del diario vivir, participábamos en diversos eventos como baile, oratoria, banda musical, folclor, entre otros, hicimos amigos y hermanos en el estudio. Consideramos que todos estos elementos nos ayudaron en nuestra formación como profesionales íntegros, hecho que se reflejó en los buenos resultados durante nuestra Práctica Profesional y ahora en nuestra vida universitaria y laboral.</p>
<p class="text-black">Sin duda, nuestros padres no se equivocaron al matricularnos en este centro y hoy damos gracias a Dios por permitirnos ser el resultado de los esfuerzos que hacen Hermanas, docentes, administrativos, padres de familia, estudiantes y amigos del ICCL, por dejar en cada estudiante la semilla del estudio, la disciplina y el amor al prójimo.</p>
   					<blockquote>
                        <p>
                            <em>
                           ÁNGELA REYES, JOSÉ RIVERA y WENDY FLORES. 
Promoción GENIUX 2018


                            </em>
                        </p>
                    </blockquote>
   				
   			</div>
   			
   			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
   			
   					<a class="lightbox" href="images/egresados-hablan-1.jpg"><img src="images/egresados-hablan-1.jpg" class="card-img-top"></a>
   					
   				
   			</div>
   		</div><!-- Termina fila-->
   		<div class="container-fluid fondo_seccion_primary">
   			<div class="row ml-1 mr-1"><!-- inicia segunda fila de aportacion de egresados-->
   			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
   			
   					<a class="lightbox" href="images/egresados-hablan-2.jpg"><img src="images/egresados-hablan-2.jpg" class="card-img-top"></a>
   					
   				
   			</div>
   			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
   				
   						<p class="text-black">El Instituto Carmen Conte Lombardo, más que ser la casa de estudio donde forjaría mis sueños de llegar a ser un profesional, era y seguirá siendo mi SEGUNDO HOGAR.
</p>
<p class="text-black">Se convirtió en el lugar donde compartía con amigos y mis segundos padres, los profesores, quienes además de ser nuestros facilitadores, me guiaban y brindaban los mejores consejos que me ayudaron en el momento y que sirven como motivación ahora en mi vida adulta.</p>
<p class="text-black">En el ICCL tuve la oportunidad de participar en el Concurso de Oratoria y como candidato a diputado juvenil, experiencias que aunadas a muchas más, recuerdo con mucha nostalgia. Extraño muchas cosas del centro, entre ellas el compañerismo, la empatía, el amor por la naturaleza, los valores y el sentido de pertenencia que sentimos como estudiantes y ahora como exalumnos.</p>
   					<blockquote>
                        <p>
                            <em>
                            FÉLIX ANTONIO BATISTA. Promoción Sudhanis 2016

                            </em>
                        </p>
                    </blockquote>
   				
   			</div>
   			
   			
   		</div>

   		</div><!-- termina div de segunda aportacion-->
   		
   		
   	</div>
   </section>

     <div class="container-fluid">
    <!-- Inicio footer-->
    <div class="row footer">
      <?php include ("sections/footer.html");?>
    </div>
  </div>
  <script src="js/jquery.js"></script>
  <script src="lib/parallax.js-1.5.0/parallax.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/baguetteBox/js/baguetteBox.min.js"></script>
  <script>baguetteBox.run('.galeria-imagenes', {
  captions: function(element) {
  return element.getElementsByTagName('img')[0].alt;
  }
  });</script>
  </body>
</html>










