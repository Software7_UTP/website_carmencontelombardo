<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="lib/fontawesome/css/all.css">
    <link rel="stylesheet" href="styles/all.css">
      

<!----ecencial para efecto swiper editado para historia------------->
<link rel="stylesheet" href="styles/historia.css.css">


    <title>Historia</title>
  </head>
  <body>
  
  <div class="container-fluid">
        <div class="row">
          <?php include "sections/menu.html"?>
        </div>
        <!-- Start Contacto-->
</div>
       
<div class="parallax" data-parallax="scroll" data-image-src="images/bg-titles-page.png">
      <h1 class="parallax-title text-center py-5 text-shadow"><b>HISTORIA</b></h1>
    </div>

<div class="container-fluid">
     <?php include ("sections/contenido_historia_a.html");?>
</div>

          <div class="container-fluid">
        <div class="row footer">
          <?php include ("sections/footer.html");?>
        </div>
      </div>  


<script src="js/jquery.js"></script>
  <script src="lib/parallax.js-1.5.0/parallax.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>

  
</body>
</html>