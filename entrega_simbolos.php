<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS - estilos que dan origininalidad a la página-->
    <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="lib/baguetteBox/css/baguetteBox.min.css">
    <link rel="stylesheet" href="lib/fontawesome/css/all.css">
    <link rel="stylesheet" href="styles/all.css">
    <title>Entrega de Símbolos</title>
  </head>
  <body>
    <!-- Barra de navegación-->
    <div class="container-fluid">
      <div class="row">
        <?php include("sections/menu.html");?>
      </div>
    </div>
    <!-- Titulo de la sección entrega de símbolos-->
    <div class="parallax" data-parallax="scroll" data-image-src="images/bg-titles-page.png">
      <h1 class="parallax-title text-center py-5 text-shadow"><b>ENTREGA DE SÍMBOLOS</b></h1>
    </div>
    
     <!-- contenedor que posee las imagenes y contenido de la sección entrega de símbolos-->
    <div class="container-fluid pt-5 bg-pr">
      <div class="container">
        <div class="galeria-imagenes">
          <div class="row"> 
            
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <a class="lightbox img-fluid" href="images/fechas-memorables/entrega-de-simbolos.jpg">
                <img src="images/fechas-memorables/entrega-de-simbolos.jpg" >
              </a>
             
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
              <a class="lightbox img-fluid" href="images/fechas-memorables/entrega-simbolos-3.jpg">
                <img src="images/fechas-memorables/entrega-simbolos-3.jpg" >
              </a>
              
            </div>
          </div>
        </div>

      </div>
    </div>

    <!-- contenido acerca de la bandera y el estardarte -->
    <div class="container-fluid fondo_seccion_primary py-5 px-0">
      <div class="container">
        <div class="row ">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="container-fluid">
              <h2>
              La Bandera:
              </h2>
              <p class="text-black">Símbolo de las grandes hazañas de los héroes  de la Patria que lucharon y entregaron su vida por mejores destinos para Panamá.  Con sus vistosos colores, representa la unificación de todas las razas bajo el lienzo tricolor.
                Hoy se las entregamos, queridos compañeros de quinto año, como signo de pertenencia.  Ámenla, respétenla y siéntanse orgullosos de ella en donde quiera que se encuentren.  Que ella nos infunda  amor y deseos de trabajar por nuestra Patria.
              </p>
              
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="container">
              <h2>
              El Estandarte:
              </h2>
              <p class="text-black">Símbolo de la identidad Lombardina.  En él se reflejan nuestras siembras y cosechas que son los frutos del futuro para nuestro País.
                Al entregarles hoy esta insignia, les pedimos que amen, respeten y representen dignamente nuestro colegio.  Continúen compañeros, sembrando y cosechando para el futuro.  Recuerden que “Del campo surgen las fuerzas vivas de la nación”.
              </p>
            </div>
            
          </div>
        </div>
      </div>
    </div>
    <!-- contenido acerca de la biblia, Niña María y Marie Poussepin -->
    <div class="container-fluid">
      <div class="container">
        <div class="row my-5">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="card border-0 transform-on-hover">
              <img src="images/fechas-memorables/entrega-de-simbolos-2.jpg"  class="img-fluid">
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 my-auto">
            <h2>La Biblia:</h2>
            <p class="text-black">Ella es la Palabra de Dios, antorcha que alumbra nuestros pasos.</p>
            <h2>Niña María:</h2>
            <p class="text-black">Símbolo de la piedad, la entrega incondicional y modelo de todas las virtudes que debemos practicar.
            </p>
            <h2>
            MARIE POUSSEPIN:
            </h2>
            <p class="text-black">Símbolo de caridad, de pureza, de sencillez, con un alma dedicada al bienestar de sus hermanos.  Plantó alegría en los corazones.  Promovió la paz y la esperanza enseñando con su ejemplo cómo caminar por la vida sin conocer el odio, basándose en el amor al prójimo.  Recíbanla, ténganla siempre presente y traten de imitar su vida de caridad.
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <div class="card border-0 transform-on-hover">
            <a href="aniversario.php"><img src="images/fechas-memorables/aniversario-carmen-conte-2.jpg" class="card-img-top"></a>
            <div class="card-body">
              <h4>Aniversario</h4>
              <a href="aniversario.php" class="btn btn-info">Ver más</a>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <div class="card border-0 transform-on-hover">
            <a class="" href="septiembre8.php"><img src="images/fechas-memorables/maria-diciembre-8.jpg" class="card-img-top"></a>
            <div class="card-body">
              <h4>8 de Septiembre</h4>
              <a href="septiembre8.php" class="btn btn-info">Ver más</a>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <div class="card border-0 transform-on-hover">
            <a class="" href="retiro_graduandos.php"><img src="images/fechas-memorables/retiro-graduandos.png" class="card-img-top"></a>
            <div class="card-body">
              <h4>Retiro de Graduandos</h4>
              <a href="retiro_graduandos.php" class="btn btn-info">Ver más</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <!-- Inicio footer-->
      <div class="row footer">
        <?php include ("sections/footer.html");?>
      </div>
      <!-- fin del footer -->
    </div>
    <script src="js/jquery.js"></script>
    <script src="lib/parallax.js-1.5.0/parallax.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="lib/baguetteBox/js/baguetteBox.min.js"></script>
    <script>baguetteBox.run('.galeria-imagenes', {
    captions: function(element) {
    return element.getElementsByTagName('img')[0].alt;
    }
    });</script>
    
  </body>
</html>