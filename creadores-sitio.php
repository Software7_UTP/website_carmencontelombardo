<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS y librerías que dan movimiento a las imágenes-->
   <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
    
    <link rel="stylesheet" href="lib/baguetteBox/css/baguetteBox.min.css">
    <link rel="stylesheet" href="images/style-gallery.css">
    <link rel="stylesheet" href="lib/fontawesome/css/all.css">
    <link rel="stylesheet" href="styles/all.css">
    <link rel="stylesheet" href="styles/proyectoeducativo_manual_convivencia.css">

    <title>Creadores del Sitio</title>
  </head>
  <body>

    <div class="container-fluid">
      <div class="row">
        <?php include("sections/menu.html");?>
      </div>
    </div>
    <!-- Titulo de la sección creadores del sitio-->
    <div class="parallax" data-parallax="scroll" data-image-src="images/bg-titles-page.png">
      <h1 class="parallax-title text-center py-5 text-shadow"><b>CREADORES DEL SITIO</b></h1>
    </div>   <!-- Inicio de creadores del sitio-->
   <section class="gallery-block galeria-imagenes">
   	<div class="container-fluid">
       <div class="row">
          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="card">
              <h3 class="card-header fondo_titulo_creadores">Créditos</h3>
              <div class="container-fluid">
                <div class="row fondo_seccion_creadores"> <!-- fila donde se encuentra los créditos de la página y los colaboradores que ayudaron con el proyecto -->
                  
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-2 mb-2">
                    
                    <p>La Universidad Tecnológica de Panamá, Centro Regional de Coclé y los estudiantes de tercer año de la carrera de Licenciatura en Desarrollo de Software - 2019, agradece el apoyo de los colaboradores del Instituto Carmen Conte Lombardo en el desarrollo del portal web dinámico <b>institutocarmencontelombardo.org</b>. 
                    Este sitio web es el proyecto final de la asignatura de Desarrollo de Software VII, a cargo de la docente Ing. María Y. Tejedor M. de Fernández.</p>
                  </div>
                </div>
              </div>
              
            </div>
            <br>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="card">
              <h3 class="card-header fondo_titulo_creadores">Colaboradores</h3>
              <div class="container-fluid">
                <div class="row fondo_seccion_creadores">
                  <!-- Lista de colaboradores de parte del Instituto Carmen Conte Lombardo-->
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-2 mb-4">
                    <ul class="list-group">
                      <li class="list-group-item text-gray">Hna. Donatila González</li>
                      <li class="list-group-item text-gray">Lic. Leonel Osorio</li>
                      <li class="list-group-item text-gray">Lic. Mario Segura</li>                  
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br>
        <!--  fila que contiene el nombre de los integrantes de cada equipo que desarrolló el portal web-->
        <h3 class="text-center text-gray">Asesora del proyecto:</h3>
        <h2 class="text-center text-gray mb-3">Ing. María Y. Tejedor M. de Fernández</h2>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 mb-4">
            <div class="card">
              <h3 class="card-header fondo_titulo_creadores text-center">Estudiantes del equipo de Interfaz</h3>
              <div class="container-fluid">
                <div class="row fondo_seccion_creadores">
                  <!-- Integrantes del equipo desarrollador de la interfaz-->
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pt-3 pb-5">
                    <ul class="list-group">
                      <li class="list-group-item list-group-item-info">Luis Sánchez</li>
                      <li class="list-group-item list-group-item-info">Gabriel Sánchez</li>
                      <li class="list-group-item list-group-item-info">Luciano Guevara</li>
                      
                    </ul>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 mb-4">
            <div class="card">
              <!-- Integrantes del equipo de Formularios-->
              <h3 class="card-header fondo_titulo_creadores text-center">Estudiantes del equipo de Formulario</h3>
              <div class="container-fluid">
                <div class="row fondo_seccion_creadores">
                  
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-2 mb-2">
                    <ul class="list-group">
                      <li class="list-group-item list-group-item-success">Heidy Calderón</li>
                      <li class="list-group-item list-group-item-success">Pascuala Hernández</li>
                      <li class="list-group-item list-group-item-success">José Escudero</li>
                      <li class="list-group-item list-group-item-success">Cristián Mendoza</li>
                      
                    </ul>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 mb-4">
            <div class="card">
              <!-- Integrantes del equipo de Base de Datos-->
              <h3 class="card-header fondo_titulo_creadores text-center">Estudiantes del equipo de Base de Datos</h3>
              <div class="container-fluid">
                <div class="row fondo_seccion_creadores">
                  
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-2 mb-2">
                    <ul class="list-group">
                      <li class="list-group-item list-group-item-warning">Saúl Solís</li>
                      <li class="list-group-item list-group-item-warning">Manuel Takata</li>
                      <li class="list-group-item list-group-item-warning">Kevin Bethancourt</li>
                      <li class="list-group-item list-group-item-warning">Félix González</li>
                      
                    </ul>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
        </div>
   		<div class="row">
   			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
   				<div class="card border-0 transform-on-hover">
   					<a class="lightbox" href="images/creadores-sitio-1.JPG"><img src="images/creadores-sitio-1.JPG" class="card-img-top"></a>
   				   				</div>
   			</div>
   			
   			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
   				<div class="card border-0 transform-on-hover">
   					<a class="lightbox" href="images/creadores-sitio-2.jpg"><img src="images/creadores-sitio-2.JPG" class="card-img-top"></a>
   					
   				</div>
   			</div>
   		</div>

   	
   	</div>
   </section>

     <div class="container-fluid">
    <!-- Inicio footer-->
    <div class="row footer">
      <?php include ("sections/footer.html");?>
    </div>
  </div>
  <script src="js/jquery.js"></script>
  <script src="lib/parallax.js-1.5.0/parallax.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/baguetteBox/js/baguetteBox.min.js"></script>
  <script>baguetteBox.run('.galeria-imagenes', {
  captions: function(element) {
  return element.getElementsByTagName('img')[0].alt;
  }
  });</script>
  </body>
</html>










