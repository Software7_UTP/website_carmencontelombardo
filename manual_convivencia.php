<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Estilos css y librerías utilizadas-->
    <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="lib/fontawesome/css/all.css">
    <link rel="stylesheet" href="styles/all.css">
    <link rel="stylesheet" href="styles/proyectoeducativo_manual_convivencia.css">
    <!-- Título de la página-->
    <title>Manual de Convivencia</title>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <?php include("sections/menu.html");?>
      </div>
    </div>
    <!-- Titulo-->
    <div class="parallax" data-parallax="scroll" data-image-src="images/bg-titles-page.png">
      <h1 class="parallax-title text-center py-5 text-shadow"><b>MANUAL DE CONVIVENCIA</b></h1>
    </div>
    <!-- Contenido -->
    <div class="container-fluid pb-3 text-style">
      <div class="row py-4">
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 ">
          <div class="container">
            <img class="img-tamano1 img-fluid" src="images/manual-convivencia-banner.png" alt="manual de convivencia">
          </div>
          
        </div>
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 m-auto">
          <div class="container">
            <p class="text-gray">Conscientes de que el sistema educativo está llamado a favorecer más que la imposición de las normas, la asimilación de valores, buscando crear convicciones en el educando, de manera que su comportamiento brote de motivaciones interiores y no dependa de la presencia o ausencia del educador, la Comunidad Educativa del Instituto Carmen Conte Lombardo, sintetiza en el Manual de Convivencia el conjunto de derechos y deberes de todos los miembros de la gran familia Lombardina.</p>
          </div>
          
        </div>
      </div>
      <!-- Descripción del manual de convivencia -->
      <div class="row fondo_seccion_primary">
        <div class="container">
          <p> Por lo anterior, en nuestra Comunidad educativa queremos proponer más que un reglamento, un MANUAL DE CONVIVENCIA ESCOLAR. En este, se presentan parámetros que sirven para regular la sana convivencia de los miembros de la comunidad educativa y está orientado a facilitar el logro de los fines del sistema educativo y los objetivos de la institución ; no hay prohibiciones de conductas, sino propuestas de asimilación de valores que favorecen el crecimiento personal y la armonía entre los  miembros de la comunidad educativa, entendiéndose como tales: estudiantes, profesores, directivos, administrativos, manuales, padres de familia.
          </p>
          <p>
            Las acciones correctivas, no constituyen un problema; el problema está más bien en las  pocas acciones reflexivas que se dan en la vida escolar y que son las que permiten el proceso de aprendizaje autónomo. La pedagogía de la corrección debe utilizarse siempre a la manera de Jesús:
          </p>
          <blockquote>
                        <p>
                            <em>
                            “Hacer que la persona se confronte a si misma con su propia conciencia.”
                            </em>
                        </p>
                        <small>
                        Juan 4,16.
                        </small>
                    </blockquote>
        </div>
      </div>
      <!--  Contenedor con la sección de Marie Poussepin-->
      <div class="container py-3">
        <div class="row">

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 ">
            <h2>MARIE POUSSEPIN</h2>
            <p class="text-bold">Desde la pedagogía de la corrección, propuesta por Marie Poussepin, es necesario concebir la norma como un camino de acompañamiento en donde se conjugan la acción correctiva como una gran disposición afectiva: </p>
           <!-- Lista de frases -->
            <ul class="list-group">
             <li class="list-group-item list-group-item-success">“Enderezadles sin llegan a cometer alguna falta”</li>
             <li class="list-group-item list-group-item-secondary">“Estad llenos de caridad”</li>
             <li class="list-group-item list-group-item-info">“Corregid sin cólera”</li>
             <li class="list-group-item list-group-item-warning">“Advertid con prudencia y dulzura sus efectos a fin de destruir el mal que de esto pudiera nacer”.</li>
           </ul>
           <br>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <img src="images/marie-poussepin.jpg" class="img-fluid img-tamano1" alt="MARIE POUSSEPIN">
            
          </div>
        </div>
      </div>
      <!-- Sección del manual de convivencia  -->
      <div class="row fondo_seccion_primary">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              
              <p>Como institución, queremos asumir el reto de educar teniendo como mira y prioridad a la persona, para que juntos persona e institución construyamos nuevos modelos educativos en donde cada uno descubra la importancia de su auto formación y auto determinación que permita desarrollar las competencias necesarias en el campo profesional, resolviendo los problemas de la comunidad. </p>
              <p>
                El Manual de Convivencia termina con una ACTA DE COMPROMISO que recoge los aspectos más sensitivos y propios del Centro Educativo, que después de estudiarlo muy bien durante la entrevista personal, firma el padre de familia, el estudiante y el director; esto permite al padre y al estudiantes conocer la filosofía, exigencias y procedimiento a seguir, una vez que acepten ingresar a la institución.
              </p>
              
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <p>Se enfatiza mucho en la disposición que debe tener el estudiante como sujeto activo de su propia formación y para ello el primer valor a cultivar es la DISCIPLINA, (hacer lo que debo hacer, cuando debo hacerlo, para llegar a ser lo que quiero ser), CIENCIA (constancia en el estudio) PIEDAD (sentido filial expresado en la fraternidad universal); además de otros valores como el sentido de pertenencia, la responsabilidad, el respeto, la sencillez etc. </p>
              
              
            </div>
          </div>
        </div>
      </div>
      <!-- CARACTERÍSTICAS Y PERFIL DEL ESTUDIANTE-->
      <div class="row py-5">
        <div class="container-fluid">
          <div class="container">
            <div class="row">
              <h2 class="text-center">CARACTERÍSTICAS Y PERFIL DEL ESTUDIANTE</h2>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <p class="text-bold">
                  De acuerdo a los cuatro pilares  fundamentales de la educación,   Planteados por la ONU, (aprender a aprender, aprender a hacer, aprender a convivir y aprender a ser) el estudiante Lombardino debe caracterizarse por:
                </p>
                <ul class="list-group">
                <li class="list-group-item list-group-item-success">Ser capaz de relacionarse consigo mismo y con los otros, con el mundo, con la historia y con Dios.</li>
                <li class="list-group-item list-group-item-secondary">Desarrollar la capacidad creadora para buscar y proponer soluciones apropiadas a la problemática de la sociedad con gran sentido de colaboración e iniciativa.</li>
                <li class="list-group-item list-group-item-info">Respetar las normas y practicar el diálogo para solucionar conflicto.</li>
                <li class="list-group-item list-group-item-warning">Valorar el grupo como un espacio para expresarse.</li>
                <li class="list-group-item list-group-item-danger">Demostrar amor y respeto por los demás.</li>
                <li class="list-group-item list-group-item-primary">Reconocer la importancia de la asimilación y vivencia de los valores Humano cristianos, indispensables para el crecimiento personal y social.</li>
              
              </ul>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 my-auto">
                
                  <ul class="list-group">
                <li class="list-group-item list-group-item-dark">Respetar y proteger el medio ambiente.</li>
                <li class="list-group-item list-group-item-light">Desarrollar hábitos y conductas deseables en la comunidad.</li>
                <li class="list-group-item list-group-item-success">Practicar hábitos de higiene y de seguridad en su vida personal y social.</li>
                <li class="list-group-item list-group-item-secondary">Promover la cultura del respeto a los derechos humanos.</li>
                <li class="list-group-item list-group-item-info">Favorecer con sus actitudes la sana convivencia.</li>
                <li class="list-group-item list-group-item-warning">Conservar y utilizar en forma adecuada los recursos naturales de modo que permita mejorar la calidad de vida.</li>
                <li class="list-group-item list-group-item-danger">Respetar a sus docentes como un tutor en su proceso de enseñanza aprendizaje y trabaja con él, asumiendo una posición clara y específica sobre el aprender.</li>
                <li class="list-group-item list-group-item-primary">Respetar la autoridad legítima: la ley, la Cultura Nacional,  la Historia Panameña y los Símbolos patrios.</li>
                <li class="list-group-item list-group-item-dark">Cultivar los valores y con sus acciones ser una persona multiplicador de los mismos promoverlos objetivamente en la sociedad de hoy.</li>
              
              </ul>


              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <!-- Inicio footer-->
    <div class="row footer">
      <?php include ("sections/footer.html");?>
    </div>
  </div>
  <!-- Librerías javascript-->
  <script src="js/jquery.js"></script>
  <script src="lib/parallax.js-1.5.0/parallax.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>