<?php

	@$nivel = $_POST['nivel'];
    @$media = $_POST['añoMedia'];
    @$preMedia = $_POST['añoPreMedia'];
    @$bachiller = $_['Bachiller']; 

 ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Filtrado</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="lib/bootstrap//css/bootstrap.min.css">
		<link rel="stylesheet" href="lib/fontawesome/css/all.css">
		<script defer src="https://use.fontawesome.com/releases/v5.11.2/js/all.js"></script>
  		<script defer src="https://use.fontawesome.com/releases/v5.11.2/js/v4-shims.js"></script>
		<link rel="stylesheet" type="text/css" href="styles/Filtrado.css">
		
	</head>
	<body>
		

		<div class="container"><!--Inicio Container-->

			<div class="container Encabezado"><!--Inicio Encabezado-->
				<label>Filtrado de Estudiantes</label>
			</div><!--Termina Encabezado-->
			<form name="formularioNivel" action="#"><!--Inicia Formulario-->
				<div class="col-12"><!--Inicio columna 1-->
					<LABEL>Filtrar estudiantes por:</LABEL>
				</div>
				

				
         

         	<div class="col-12 forgot"><!--Inicio columna 1-->
					
                <div  data-toggle="buttons">
		         <label class="btn">
		            <input type="radio" name="filtrar" id="option1" autocomplete="off" value="4" onchange="habilitarAño(this.value)">Beneficio
		        </label>

		        <label class="btn">
		            <input type="radio" name="filtrar" id="option2" autocomplete="off" value="5" onchange="habilitarAño(this.value)">Beca
		        </label> 
		        <label class="btn">
		            <input type="radio" name="filtrar" id="option3" autocomplete="off" value="6" onchange="habilitarAño(this.value)">Discapacidad
		        </label><br><br>
		        Por Sexo:
		        <div  data-toggle="buttons">
		        	<label class="btn">
		            <input type="radio" name="filtrar" id="option3" autocomplete="off" value="7" onchange="habilitarAño(this.value)">Masculino
		        </label>

		        <label class="btn">
		            <input type="radio" name="filtrar" id="option3" autocomplete="off" value="8" onchange="habilitarAño(this.value)">Femenino
		        </label>
		        </div>

		        
		    </div> <br>

		    Por Nivel:

		    <div  data-toggle="buttons">
		         <label class="btn">
		            <input type="radio" name="filtrar" id="option1" autocomplete="off" value="1" onchange="habilitarAño(this.value)">Pre Media
		        </label>

		        <label class="btn">
		            <input type="radio" name="filtrar" id="option2" autocomplete="off" value="3" onchange="habilitarAño(this.value)">Media
		        </label> 

		        <label class="btn">
		            <input type="radio" name="filtrar" id="option3" autocomplete="off" value="2" onchange="habilitarAño(this.value)">Todos
		        </label><br><br>
		        Tipo de Estudiante:
		        <div  data-toggle="buttons">
		        	<label class="btn">
		            	<input type="radio" name="filtrar" id="option3" autocomplete="off" value="9" onchange="habilitarAño(this.value)">Regular
		        	</label>
		        	<label class="btn">
		            	<input type="radio" name="filtrar" id="option3" autocomplete="off" value="10" onchange="habilitarAño(this.value)">Reprobado
		        	</label>
		        </div>
		    </div>
                
	            </div><!--Termina columna 1-->
         
         	

		    <div class="col-12 forgot"><!--Inicia columna 2-->

		        <a href="sections/Imprimir/imprimir-pdf.php"  target="_blank"><button type="button" class="btn" name="btnNivel" id="btnNivel" hidden="true">Imprimir</button></a>

         		<a href="sections/Imprimir/imprimir-pdf2.php"  target="_blank"><button type="button" class="btn" name="btnNivel2" id="btnNivel2" hidden="true">Imprimir</button></a>

         		<a href="sections/Imprimir/imprimir-pdf3.php"  target="_blank"><button type="button" class="btn" name="btnNivel3" id="btnNivel3" hidden="true">Imprimir</button></a>

         		<a href="sections/Imprimir/imprimir-pdf4.php"  target="_blank"><button type="button" class="btn" name="btnNivel4" id="btnNivel4" hidden="true">Imprimir</button></a>

         		<a href="sections/Imprimir/imprimir-pdf5.php"  target="_blank"><button type="button" class="btn" name="btnNivel5" id="btnNivel5" hidden="true">Imprimir</button></a>
         		<a href="sections/Imprimir/imprimir-pdf6.php"  target="_blank"><button type="button" class="btn" name="btnNivel6" id="btnNivel6" hidden="true">Imprimir</button></a>

         		<a href="sections/Imprimir/imprimir-pdf7.php"  target="_blank"><button type="button" class="btn" name="btnNivel7" id="btnNivel7" hidden="true">Imprimir</button></a>

         		<a href="sections/Imprimir/imprimir-pdf8.php"  target="_blank"><button type="button" class="btn" name="btnNivel8" id="btnNivel8" hidden="true">Imprimir</button></a>

         		<a href="sections/Imprimir/imprimir-pdf9.php"  target="_blank"><button type="button" class="btn" name="btnNivel9" id="btnNivel9" hidden="true">Imprimir</button></a>

         		<a href="sections/Imprimir/imprimir-pdf10.php"  target="_blank"><button type="button" class="btn" name="btnNivel10" id="btnNivel10" hidden="true">Imprimir</button></a>
                   
                </div><!--Termina columna 2-->

			</form><!--Termina Formulario-->

			
			 
         		

			
		</div><!--Termina Container-->

	    <script type="text/javascript">

	    	//Función para mostrar y ocultar los elemenstos dependiendo de la opción que escoga el usuario.
	    	//Se toma como parámetro el value del comboBox "Nivel".
	    	function habilitarAño(value){
	    		//Si la opción que escogió fue "Seleccionar Nivel", no se muestra ningún comboBox
	    		if (value=='0'){
                    
                    document.getElementById('btnNivel').hidden = true;
                    document.getElementById('btnNivel2').hidden = true;
                    document.getElementById('btnNivel3').hidden = true;
                    document.getElementById('btnNivel4').hidden = true;
                    document.getElementById('btnNivel5').hidden = true;
                    document.getElementById('btnNivel6').hidden = true;
                    document.getElementById('btnNivel7').hidden = true;
                    document.getElementById('btnNivel8').hidden = true;
                    document.getElementById('btnNivel9').hidden = true;
                    document.getElementById('btnNivel10').hidden = true;

                                
	    		}

	    		//Si la opción que ecogió fue "Pre-Media" se muestra el comboBox con los grados de pre-media 
	    		//y la tabla respectiva.
	    		if (value=='1'){
	    			      
                    document.getElementById('btnNivel').hidden = false;
                    document.getElementById('btnNivel2').hidden = true;
                    document.getElementById('btnNivel3').hidden = true;
                    document.getElementById('btnNivel4').hidden = true;
                    document.getElementById('btnNivel5').hidden = true;
                    document.getElementById('btnNivel6').hidden = true;
                    document.getElementById('btnNivel7').hidden = true;
                    document.getElementById('btnNivel8').hidden = true;
                    document.getElementById('btnNivel9').hidden = true;
                    document.getElementById('btnNivel10').hidden = true;

	    		}

	    		//Si la opción que ecogió fue "Media" se muestra el comboBox con los grados de media 
	    		//y la tabla respectiva.
	    		if (value=='2'){

	    			document.getElementById('btnNivel2').hidden = true;
                    document.getElementById('btnNivel').hidden = true; 
                    document.getElementById('btnNivel3').hidden = false;
                    document.getElementById('btnNivel4').hidden = true;
                    document.getElementById('btnNivel5').hidden = true;
                    document.getElementById('btnNivel6').hidden = true;
                    document.getElementById('btnNivel7').hidden = true;
                    document.getElementById('btnNivel8').hidden = true;
                    document.getElementById('btnNivel9').hidden = true;
                    document.getElementById('btnNivel10').hidden = true;
                   
	    		}

	    		//Si la opción que ecogió fue "Todos" se muestra la tabla con todos los registros
	    		if (value=='3'){	    			
                    document.getElementById('btnNivel3').hidden = true;
                    document.getElementById('btnNivel').hidden = true;
                    document.getElementById('btnNivel2').hidden = false;
                    document.getElementById('btnNivel4').hidden = true;
                    document.getElementById('btnNivel5').hidden = true;
                    document.getElementById('btnNivel6').hidden = true;
                    document.getElementById('btnNivel7').hidden = true;
                    document.getElementById('btnNivel8').hidden = true;
                    document.getElementById('btnNivel9').hidden = true;
                    document.getElementById('btnNivel10').hidden = true;
	    		}

	    		if (value=='4'){	    			
                    document.getElementById('btnNivel3').hidden = true;
                    document.getElementById('btnNivel').hidden = true;
                    document.getElementById('btnNivel2').hidden = true;
                    document.getElementById('btnNivel4').hidden = false;
                    document.getElementById('btnNivel5').hidden = true;
                    document.getElementById('btnNivel6').hidden = true;
                    document.getElementById('btnNivel7').hidden = true;
                    document.getElementById('btnNivel8').hidden = true;
                    document.getElementById('btnNivel9').hidden = true;
                    document.getElementById('btnNivel10').hidden = true;
	    		}

	    		if (value=='5'){	    			
                    document.getElementById('btnNivel3').hidden = true;
                    document.getElementById('btnNivel').hidden = true;
                    document.getElementById('btnNivel2').hidden = true;
                    document.getElementById('btnNivel4').hidden = true;
                    document.getElementById('btnNivel5').hidden = false;
                    document.getElementById('btnNivel6').hidden = true;
                    document.getElementById('btnNivel7').hidden = true;
                    document.getElementById('btnNivel8').hidden = true;
                    document.getElementById('btnNivel9').hidden = true;
                    document.getElementById('btnNivel10').hidden = true;
	    		}

	    		if (value=='6'){	    			
                    document.getElementById('btnNivel3').hidden = true;
                    document.getElementById('btnNivel').hidden = true;
                    document.getElementById('btnNivel2').hidden = true;
                    document.getElementById('btnNivel4').hidden = true;
                    document.getElementById('btnNivel5').hidden = true;
                    document.getElementById('btnNivel6').hidden = false;
                    document.getElementById('btnNivel7').hidden = true;
                    document.getElementById('btnNivel8').hidden = true;
                    document.getElementById('btnNivel9').hidden = true;
                    document.getElementById('btnNivel10').hidden = true;
	    		}

	    		if (value=='7'){	    			
                    document.getElementById('btnNivel3').hidden = true;
                    document.getElementById('btnNivel').hidden = true;
                    document.getElementById('btnNivel2').hidden = true;
                    document.getElementById('btnNivel4').hidden = true;
                    document.getElementById('btnNivel5').hidden = true;
                    document.getElementById('btnNivel6').hidden = true;
                    document.getElementById('btnNivel7').hidden = false;
                    document.getElementById('btnNivel8').hidden = true;
                    document.getElementById('btnNivel9').hidden = true;
                    document.getElementById('btnNivel10').hidden = true;
	    		}

	    		if (value=='8'){	    			
                    document.getElementById('btnNivel3').hidden = true;
                    document.getElementById('btnNivel').hidden = true;
                    document.getElementById('btnNivel2').hidden = true;
                    document.getElementById('btnNivel4').hidden = true;
                    document.getElementById('btnNivel5').hidden = true;
                    document.getElementById('btnNivel6').hidden = true;
                    document.getElementById('btnNivel7').hidden = true;
                    document.getElementById('btnNivel8').hidden = false;
                    document.getElementById('btnNivel9').hidden = true;
                    document.getElementById('btnNivel10').hidden = true;
	    		}

	    		if (value=='9'){	    			
                    document.getElementById('btnNivel3').hidden = true;
                    document.getElementById('btnNivel').hidden = true;
                    document.getElementById('btnNivel2').hidden = true;
                    document.getElementById('btnNivel4').hidden = true;
                    document.getElementById('btnNivel5').hidden = true;
                    document.getElementById('btnNivel6').hidden = true;
                    document.getElementById('btnNivel7').hidden = true;
                    document.getElementById('btnNivel8').hidden = true;
                    document.getElementById('btnNivel9').hidden = false;
                    document.getElementById('btnNivel10').hidden = true;
	    		}

	    		if (value=='10'){	    			
                    document.getElementById('btnNivel3').hidden = true;
                    document.getElementById('btnNivel').hidden = true;
                    document.getElementById('btnNivel2').hidden = true;
                    document.getElementById('btnNivel4').hidden = true;
                    document.getElementById('btnNivel5').hidden = true;
                    document.getElementById('btnNivel6').hidden = true;
                    document.getElementById('btnNivel7').hidden = true;
                    document.getElementById('btnNivel8').hidden = true;
                    document.getElementById('btnNivel9').hidden = true;
                    document.getElementById('btnNivel10').hidden = false;
	    		
	    	}  
	    	}            
        </script>

	

		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

		<script type="text/javascript" language="javascript" src="scriptaculous/lib/prototype.js"></script>
<script type="text/javascript" language="javascript" src="scriptaculous/src/scriptaculous.js"></script>
	</body>
</html>