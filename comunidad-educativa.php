<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="lib/baguetteBox/css/baguetteBox.min.css">
		<link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="lib/fontawesome/css/all.css">
		<link rel="stylesheet" type="text/css" href="styles/all.css">
		<link rel="stylesheet" type="text/css" href="styles/comunidad-educativa-css.css">
		<title>Comunidad Educativa</title>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<?php include ("sections/menu.html");?>
			</div>
		</div>
		<!--Título Página  images/img-comunidad_educativa/bg-comunidad-educativa.jpg -->
		<div class="parallax" data-parallax="scroll" data-image-src="images/bg-titles-page.png">
			<h1 class="parallax-title text-center  text-shadow"><b>COMUNIDAD EDUCATIVA</b></h1>
		</div>
		<!-- Inicio contenido-->
		<div class="container pt-4">
			<!--Estudiantes-->
			<h2>Estudiantes</h2>
			<p>El estudiante Lombardino se caracteriza por su sentido de pertenencia. Es decir  valora, quiere, defiende, cuida todo lo que la institución pone a sus servicios.</p>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 my-auto">
					<img src="images/img-comunidad_educativa/estudiantes-carmen-conte-lombardo.jpg" class="img-fluid" alt="Responsive image">
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 align-items-center">
					<p><b>Para expresar el sentido de pertenencia el estudiante: </b></p>
					<ul id="ul-com-edu">
						<li class="li-com-edu">Respeta y honra los Símbolos Patrios.</li>
						<li class="li-com-edu">Cuida y valora la infraestructura, equipo mobiliario y áreas verdes.</li>
						<li class="li-com-edu">Reporta cualquier daño contra la infraestructura y mobiliario dentro del plantel</li>
						<li class="li-com-edu">Mantiene limpios los predios y áreas escolares, colocando la basura en el sitio correcto.</li>
						<li class="li-com-edu">Se aprende el himno del colegio y cantarlo con fervor</li>
					</ul>
				</div>
			</div>
		</div>
		<!--Docentes-->
		<div class="container">
			<h2>Docentes</h2>
			<div class="row">
				<div class="container-fluid">
					<img src="images/img-comunidad_educativa/docentes-profesores-instituto-lombardo.jpg" class="img-fluid" alt="Docentes del instituto Carmen Conte Lombardo">				
					<p>El docente debe constar con estas cualidades: académica, actitudinal y social,  es un transmisor de conocimiento, un ejemplo a seguir, es el que permite que haya en la sociedad persona humanas académicamente formadas que sirven a la sociedad.</p>
					<ul>
						<li class="li-com-doce">Debe ser docente formador proactivo: que con su ejemplo de vida es capaz de guiar, orientar, aconsejar y enseñar valores, a sus estudiantes y estos responder asertivamente a los problemas que se suscitan en la sociedad hoy.</li>
						<li class="li-com-doce">Debe ser un docente responsable, consciente, progresivo, que conoce el proceso educativo, los contenidos de su área curricular,  las técnicas y estrategias que  mejoren el aprendizaje en sus alumnos.</li>
						<li class="li-com-doce">Debe ser un docente actualizado, que constantemente adquiere información para fortalecer los conocimientos adquiridos  e innovar cada día sus clases, conoce los avances tecnológicos y  las últimas técnicas e innovaciones pedagógicas que necesitan nuestros estudiantes para el proceso de enseñanza aprendizaje.</li>
					</ul>
				</div>
			</div>
			
			<div class="galeria-imagenes">
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<a class="lightbox" href="images\img-comunidad_educativa/convivencia-jesus-maestro.jpg">
							<img src="images\img-comunidad_educativa/convivencia-jesus-maestro.jpg" alt="Convivencia: Jesús Maestro.">
						</a>
					</div>
					<div class="col-sm-6 col-md-6">
						<a class="lightbox" href="images\img-comunidad_educativa/capacitacion-institucional.jpg">
							<img src="images\img-comunidad_educativa/capacitacion-institucional.jpg" alt="Capacitación Institucional." >
						</a>
					</div>
				</div>
			</div>
			
		</div>
		<div class="container-fluid " id="fondo-cita">
			<div class="container py-5" id="fondo-cita-container">
				<div class="row">
					<div class="col-xs-12 col-md-3 mx-auto pb-3">
						<img src="images\img-comunidad_educativa/papa-francisco.jpg" class="img-fluid ">
					</div>
					<div class="col-xs-12  col-md-9" id="comillas">
						<p>El educador en las escuelas católicas debe ser ante todo muy competente, calificado, y al mismo tiempo lleno de humanidad, capaz de estar entre los jóvenes con estilo pedagógico, para promover su crecimiento humano y espiritual.</p>
						<span>Papa Francisco</span>
					</div>
				</div>
			</div>
		</div>
		<!--Comunidad Religiosa-->
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-7">
					<h2>Comunidad Religiosa</h2>
					<p>Las Hermanas de la Caridad Dominicas de la Presentación de la   Santísima Virgen. Somos una Congregación religiosa internacional, presente en cuatro continentes. Que en la rica gama de culturas de 36 países, estamos presentes en escuelas, colegios, universidades, procurando la formación integral cristiana de niños, jóvenes y adultos.</p>
					<p>Educar la niñez y la juventud, se convierte en oportunidad para permitir que el Carisma de Marie Poussepin comience su siembra y haga posible que un buen número de estudiantes conozca y aprecie la verdad como una manera de aproximarse a la ciencia, sin olvidar su dimensión de hijo de Dios.  No solo se imparten conocimientos intelectuales, sino que brinda formación integral que les permite situarse en el contexto de la realidad.</p>
					<p>Vivir y anunciar a Jesucristo como consagradas, a través de nuestra vivencia fraterna, la apertura a la pluriculturalidad y la cogida entre nosotras y con los destinatarios de la misión.</p>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 my-auto">
					<img src="images/img-comunidad_educativa/comunidad-religiosa.jpg" class="img-fluid" alt="Responsive image">
				</div>
				
				<div class="container-fluid">
					<p>“La comunidad mirará siempre como uno de sus principales deberes la instrucción y la educación de la juventud”. Marie Poussepin</p>
					<p>Historia y Misión son dos realidades teológicas muy importantes para quien vive de fe.  Nosotras como partícipes de un Carisma concedido a la Iglesia por el Espíritu, somos agentes del Reino de Dios que existimos para la Misión, la cual se realiza en el acontecer de cada día. </p>
					
					
					<p>Es notable la disciplina de trabajo que las Hermanas, Personal Docente y Administrativo y el Equipo que colabora en la formación saben impartir a los destinatarios de la misión, así lo reconocen y valoran las distintas autoridades de Educación. </p>
					<p>Es maravilloso ver como el Carisma de Nuestra Fundadora tiene en todas las latitudes respuestas concretas y posibles para que quienes quieren hacerlo vida tengan la oportunidad de asumirlo desde el compartir con las Hermanas, que ya han decidido vivirlo “allí donde la Iglesia las llame y los Hermanos las necesiten” (25 Años de Historia Provincia de Medellín)</p>
					<p>En Panamá hacemos presencia en la ciudad capital con dos comunidades, una en San Francisco, que funciona como casa de acogida y el Centro Educativo Marie Poussepin que tiene desde pre escolar hasta bachillerato en ciencias y Turismo; en Atalaya, Veraguas, con una comunidad al servicio de la parroquia; en El Bale de Ñurun, zona comarcal, con una Comunidad en el Centro Misionero Nuestra Señora de Guadalupe y en Churuquita Chiquita, Penonomé, una comunidad en el Instituto Carmen Conte Lombardo.</p>
				</div>
				<div class="card mb-12" >
					<div class="row no-gutters">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 my-auto">
							<img src="images/img-comunidad_educativa/comunidad-religiosa-2.jpg" class="card-img" alt="...">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
							<div class="card-body">
								<h3 class="text-center">Una comunidad internacional,  integrada por 4 hermanas</h3>
								<p><b>Hna Eva Ignacia Berrocal Solano: </b>Docente responsable de la pastoral juvenil- vocacional</p>
								<p><b>Hna Donatila González Gómez: </b>Directora del Instituto Carmen Conte Lombardo, miembro de la Comisión Diocesana de Catequesis y del Equipo Nacional de Innovación Curricular del Ministerio de Educación.</p>
								<p><b>Hna Berta Ligia Monsalve Cogollo: </b>Docente y coordinadora académica.</p>
								<p><b>Hna Teresa Londoño Restrepo: </b>Responsable de la cafetería del colegio y pastoral de la salud, visita a los enfermos y les lleva la Sagrada Comunión.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--Personal Administrativo-->
		<div class="container">
			<h2>Personal administrativo</h2>..
			<p>El personal Administrativo y de Servicios Generales son muy importantes en la buena marcha de la instituto Carmen Conte Lombardo de quienes conforman la Comunidad Educativa.  se caracteriza por:</p>
			<ul>
				<li>Tener gran sentido de pertenencia, entrega y generosidad, buenas relaciones humanas,  colabora en las actividades de la Institución.</li>
				<li>Mantiene un espíritu de superación de acuerdo a los avances tecnológicos y los aplican a sus labores diarias a los que dan un toque de originalidad, sencillez y alegría.</li>
				<li>Hace de su lugar de trabajo un espacio agradable, limpio y ordenado; lo que propicia un clima de trabajo armónico, colaborativo y respetuoso.</li>
			</ul>
			<div class="row">
				<div class="col-md-4 text-white">
					<div class="card mb-4  bg-docentes" >
						<img class="card-img-top" src="images/img-comunidad_educativa/padres-familia-lombardo.png" alt="Card image cap">
						<div class="card-body ">
							<p class="text-white">El personal de servicios generales cumple una labor importante en la institución, pues ellos tienen la misión de facilitar el normal desarrollo de las actividades escolares para que el proceso enseñanza aprendizaje sea agradable e invite a los jóvenes estudiantes a sentir el colegio como su segundo hogar, donde quieran  permanecer.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card mb-4 border-dark bg-docentes">
						<img class="card-img-top" src="images/img-comunidad_educativa/comunidad-educativa-2.jpg" alt="Card image cap">
						<div class="card-body">
							<p  class="text-white">El Colegio no es sólo el lugar donde trabajamos, sino el espacio donde compartimos la vida, la fe y la esperanza de forjar un mundo mejor, a través del trabajo sencillo, honesto y responsable. </p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card mb-4 text-white bg-docentes">
						<img class="card-img-top" src="images/img-comunidad_educativa/comunidad-educativa-3.jpg" alt="Card image cap">
						<div class="card-body">
							<p  class="text-white">Aquí reconocemos los valores y premiamos la generosidad y entrega de nuestro personal.</p>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--Padres de Familia-->
		<div class="container">
			<h2>Padres de Familia</h2>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<img src="images/img-comunidad_educativa/padres-familia-educativa-2.jpg" class="img-fluid" alt="padres el Instituto Carmen Conte Lombardo">
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<p>Para lograr una labor educativa integral en nuestra institución necesitamos Padres de familia comprometidos, vigilantes del proceso de enseñanza aprendizaje, de los quehaceres académicos en el hogar y en la institución que involucren a sus hijos. Con sentido de pertenencia, consciente de trabajar en equipo y de valorar los viene y patrimonio de la institución. </p>
					<p>Nuestro Centro Educativo cuenta con padres de familia de diferentes lugares de la provincia, muchos de ellos con un grado de formación académica. Esto favorece a que se preocupen por una educación de calidad en sus hijos, acorde a la formación humana integral que exige la sociedad de hoy. Por tanto, el padre de familia que accede a esta institución debe poseer una actitud que reflejen valores de escucha y dialogo entre institución educativa y padres de familia, como primeros agentes formadores para sus hijos. </p>
				</div>
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
							<div class="card" >
								<img src="images/img-comunidad_educativa/lema-title-comuedu.png" class="img-fluid" alt="...">
								<div class="card-body">
									<p class="card-text">OBSERVAR, ESCUCHAR Y RESOLVER </p>
								</div>
							</div>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
							<div class="card" >
								<img src="images/img-comunidad_educativa/mision-title-comuedu.png" class="img-fluid" alt="...">
								<div class="card-body">
									<p class="card-text">Acompañar a los hijos en el proceso de enseñanza aprendizaje, desarrollando en ellos buenas prácticas valores en la convivencia, por la cual se distingue la institución; que como parte de la Iglesia busca la formación de seres humanos íntegros, que sean agentes de cambios en sus comunidades, capaces de enfrentar los retos y compromisos de una sociedad en continuos cambios. </p>
								</div>
							</div>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
							<div class="card" >
								<img src="images/img-comunidad_educativa/vision-title-comuedu.png" class="img-fluid" alt="...">
								<div class="card-body">
									<p class="card-text">Que la Asociación de Padres de familia tenga sentido de pertenencia, apoye a la Institución en toda su tarea de formación integral.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Fin Contenido-->
		<!-- Footer-->
		<div class="container-fluid">
			<div class="row footer">
				<?php include ("sections/footer.html");?>
			</div>
		</div>
		
		
		<script src="js/jquery.js"></script>
		<script src="lib/baguetteBox/js/baguetteBox.min.js"></script>
		<script>baguetteBox.run('.galeria-imagenes', {
		captions: function(element) {
		return element.getElementsByTagName('img')[0].alt;
		}
		});</script>
		<script src="lib/parallax.js-1.5.0/parallax.min.js"></script>
		<script src="lib/bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>