<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="lib/fontawesome/css/all.css">
		<link rel="stylesheet" href="styles/all.css">
		<link rel="stylesheet" href="styles/bachiller-ciencias.css">
		<title>Bachiller en ciencias</title>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<?php include ("sections/menu.html");?>
			</div>
		</div>
		<!-- Titulo-->
		

		<div class="container-fluid">
			<!-- Inicio footer-->
			<div class="row footer">
				<?php include ("Mostrar.php");?>
			</div>
		</div>
		<!-- formulario -->
		

		<div class="container-fluid">
			<!-- Inicio footer-->
			<div class="row footer">
				<?php include ("sections/footer.html");?>
			</div>
		</div>

		<script src="js/jquery.js"></script>
		<script src="lib/parallax.js-1.5.0/parallax.min.js"></script>
		<script src="lib/bootstrap/js/bootstrap.min.js"></script>
		
	</body>
</html>