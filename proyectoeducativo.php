<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Librerías y css que dan estilo a la página-->
    <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="lib/fontawesome/css/all.css">
    <link rel="stylesheet" href="styles/all.css">
    <link rel="stylesheet" href="styles/proyectoeducativo_manual_convivencia.css">
    <!-- Título de la página-->
    <title>Proyecto Educativo</title>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <?php include("sections/menu.html");?>
      </div>
    </div>
    <!-- Titulo-->
    <div class="parallax" data-parallax="scroll" data-image-src="images/bg-titles-page.png">
      <h1 class="parallax-title text-center py-5 text-shadow"><b>PROYECTO EDUCATIVO</b></h1>
    </div>
    <!-- Contenido de la sección proyecto educativo -->
    <div class="container-fluid pb-3">
      <div class="row py-4">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ">
          <div class="container">
            <img class="img-tamano1 img-fluid" src="images/textos-proyecto-educativo.png" alt="proyecto educativo">
          </div>
          
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 m-auto">
          <div class="container">
            <p class="text-gray">El Proyecto Educativo de Centros (PEC) es el camino pedagógico y administrativo del Instituto Carmen Conte que se proyecta llevar a cabo durante los próximos cuatro (4) años.
            El mismo lo hemos realizado los miembros de la Comunidad Educativa, teniendo en cuenta el PEC (Proyecto Educativo de Centro) que ha orientado la Institución los últimos años y el PIMCE (Programa Integral de Mejoramiento del Centro Educativo). El mismo tiene como marco general la Ley Orgánica de Educación, los lineamientos de la Diócesis de Penonomé, los Principios de la Iglesia Católica y la Pedagogía de Marie Poussepin, fundadora de las Hermanas Dominicas de la Presentación.</p>
          </div>
          
        </div>
      </div>
      <!-- Descripción del proyecto educativo -->
      <div class="row fondo_seccion_secondary">
        <div class="container">      
             <p>
                Este PEC se asume como un proceso de reflexión permanente, participativa y colectiva de experiencia humana y académica de todos los miembros de la comunidad educativa, sobre los fundamentos que orientan y dan sentido al quehacer de nuestra Institución educativa de paso con los avances técnico científico del mundo de hoy.  En éste se reconstruye, se recrea y sistematiza la historia de nuestra institución y se encuentra una alternativa para incrementar el desarrollo que nos motiva a comprometernos de forma integral.
             </p>
             <p>
               Nuestra Institución, creada por inquietud de la Iglesia panameña, asume el Proyecto Educativo de la escuela católica en donde los principios evangélicos se convierten en normas educativas para el bien de todo el personal que convive en ella.  De esta manera, adquirimos conciencia de nuestro empeño por promover a la persona de manera integral, mediante el cultivo de los valores, que elevan y ennoblecen al hombre.
             </p>
        </div>
      </div>
      <!--  Estudiantes del Proyecto Educativo-->
      <div class="container py-3">
        <div class="row">

          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
             <p>
               Por lo anterior, sentimos la necesidad de asegurar que el Proyecto Educativo y el Evangelio permeabilice toda la vida de todos los agentes que allí comparten el quehacer educativo y vigilar para que las tareas: académico-pedagógica, pastoral y gerencial lleguen a armonizarse hasta que sean expresión de la calidad del servicio que ofrecemos.
             </p>
              <p>
              Por nuestra tendencia a la formación integral de la persona, (Ley orgánica de educación artículo 1b) somos conscientes de que nuestro Proyecto Educativo, además de atender a la parte de infraestructura, fundamentalmente se centra en la atención, formación y evangelización de los estudiantes.
               </p>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
            <img src="images/estudiantes-proyecto-educativo.jpg" class="img-fluid" alt="Estudiantes en el Proyecto Educativo">
            
          </div>
        </div>
      </div>
      <!-- Seccion del proyecto educativo  -->
      <div class="row fondo_seccion_primary">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              
              <p>Como institución, queremos asumir el reto de educar teniendo como mira y prioridad a la persona, para que juntos persona e institución construyamos nuevos modelos educativos en donde cada uno descubra la importancia de su auto formación y auto determinación que permita desarrollar las competencias necesarias en el campo profesional, resolviendo los problemas de la comunidad. </p>
              <p>
                El Manual de Convivencia termina con una ACTA DE COMPROMISO que recoge los aspectos más sensitivos y propios del Centro Educativo, que después de estudiarlo muy bien durante la entrevista personal, firma el padre de familia, el estudiante y el director; esto permite al padre y al estudiantes conocer la filosofía, exigencias y procedimiento a seguir, una vez que acepten ingresar a la institución.
              </p>
              
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <p>Se enfatiza mucho en la disposición que debe tener el estudiante como sujeto activo de su propia formación y para ello el primer valor a cultivar es la DISCIPLINA, (hacer lo que debo hacer, cuando debo hacerlo, para llegar a ser lo que quiero ser), CIENCIA (constancia en el estudio) PIEDAD (sentido filial expresado en la fraternidad universal); además de otros valores como el sentido de pertenencia, la responsabilidad, el respeto, la sencillez etc. </p>
              
              
            </div>
          </div>
        </div>
      </div>
      <!-- Misión y Visión del Instituto-->
      <div class="row py-5">
        <div class="container-fluid">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
               <div class="card border-dark mb-8">
                        <img alt="..." class="card-img-top" src="images/img-comunidad_educativa/mision-title-comuedu.png"></img>
                            <div class="card-body">
                              <!-- Misión -->
                                <p class="card-text">
                                    Contribuir al crecimiento integral de la persona, ofreciendo una formación académica y en valores; que permita el pleno desarrollo de la comunidad educativa, iluminados por la doctrina católica, la pedagogía de Marie Poussepin y los lineamientos  del MEDUCA.
                                </p>
                            </div>
                        
                    </div>
                    <br>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                
                 <div class="card border-dark mb-3">
                        <img alt="..." class="card-img-top" src="images/img-comunidad_educativa/vision-title-comuedu.png"></img>
                            <div class="card-body">
                              <!-- Visión-->
                                <p class="card-text">
                                    Que el joven llegue a ser agente de cambio en la sociedad e impulse la armonía social, reconciliador permanente; un hombre humano con capacidad de amar la vida como don de Dios.
                                </p>
                            </div>
                        
                    </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <!-- Inicio footer-->
    <div class="row footer">
      <?php include ("sections/footer.html");?>
    </div>
  </div>
  <!--Librerías javascript -->
  <script src="js/jquery.js"></script>
  <script src="lib/parallax.js-1.5.0/parallax.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>