<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="lib/fontawesome/css/all.css">
		<link rel="stylesheet" href="styles/all.css">
		<link rel="stylesheet" href="styles/bachiller-ciencias.css">
		<title>Bachiller en ciencias</title>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<?php include ("sections/menu.html");?>
			</div>
		</div>
		<!-- Titulo-->
		<div class="parallax" data-parallax="scroll" data-image-src="images/bg-titles-page.png">
			<h1 class="parallax-title text-center py-5 text-shadow"><b>BACHILLER EN CIENCIAS</b></h1>
		</div>
		<!-- Contenido -->
		<div class="container-fluid bg-pr">
			<div class="container">
				<div class="row flex-column-reverse flex-md-row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6  my-auto">
						<h2 class="text-section-title text-gray"> Las ciencias naturales, el conjunto de valores éticos, espirituales y morales que complementan a un individuo, del cual se requiere una alta capacidad analítica, comprensiva y humana, son nuestros ejes fundamentales en la enseñanza que se imparte en nuestro colegio.</h2>
						<a class="btn btn-danger mx-auto mb-3" href="sections/plan-de-estudio-bachiller-en-ciencias.pdf" target="_blank">Ver plan de estudio</a>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<img class="img-fluid" src="images/img-bachiller-ciencias/ciencias-header.png" alt="educación en valores">
					</div>
				</div>
			</div>

		</div>
		<div class="container-fluid fondo_seccion_primary">
			<div class="container">
				<div class="row px-3">
					<p>El aprendizaje práctico consiste en simulaciones de casos prácticos según el conocimiento teórico que van adquiriendo los alumnos en las aulas. Este tipo de aprendizaje  busca no solo poner en práctica lo que uno estudia sino que se aprenda directamente al ponerse en la situación y el contexto. </p>
					<p>Potencia la iniciativa individual, el trabajo en equipo e incrementa la dimensión social de interrelación entre compañeros.</p>
					<p>El aprendizaje práctico es un estímulo al ciclo regular de estudios y es una experiencia que convierte el proceso de aprendizaje en un reto activo lleno de intercambio de ideas experiencias que permite el acercamiento del alumno al mundo en que se desenvuelve.  </p>
					<h3>Por ello impartimos una enseñanza basada en los experimentos.</h3>
				</div>
			</div>
		</div>
		<div class="container-fluid my-4">
			<h2 class="text-center text-gray">El aprendizaje práctico nos permite obtener múltiples ventajas:</h2>
			<div class="container">
				<div class="row py-3 m-auto text-center justify-content-center">
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-2">
						<div class="card-body">
							<i class="fas icono-contenedor-bc fa-brain fa-5x" aria-hidden="true"></i>
							<p>Aprende observación y análisis</p>
						</div>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 ">
						<div class="card-body">
							<i class="fas icono-contenedor-bc fa-chalkboard-teacher fa-5x" aria-hidden="true"></i>
							<p>Mejora el interés en el alumno por aprender</p>
						</div>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-2">
						<div class="card-body">
							<i class="fas icono-contenedor-bc fa-user-friends fa-5x" aria-hidden="true"></i>
							<p>Aprende a trabajar en grupo de forma disciplinada</p>
						</div>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-2">
						<div class="card-body">
							<i class="fab icono-contenedor-bc fa-leanpub fa-5x" aria-hidden="true"></i>
							<p>Fija el aprendizaje teórico </p>
						</div>
						
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-2">
						<div class="card-body">
							<i class="fas fa-user-plus icono-contenedor-bc fa-5x" aria-hidden="true"></i>
							<p>Socialización</p>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid pt-4 linea-diagonal" id="bg-giras" >
			<div class="container">
				<h2 class="text-white mt-5">Giras Académicas</h2>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 my-auto">
						<h3 class="text-white text-secondary-title">Las Giras académicas tienen como objetivos ampliar los conocimientos de los estudiantes directamente con el contexto, permitiéndole convivir, despertar el interés en otros campos de la ciencia, aplicar conocimientos adquiridos, analizar las ventajas y desventajas de un comportamiento no acorde al equilibrio natural.</h3>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
							</ol>
							<div class="carousel-inner">
								<div class="carousel-item active">
									<img class="d-block w-100" src="images/img-bachiller-ciencias/estudiantes-en-biomuseo-2.jpg" alt="biomuseo de Panamá">
									<div class="card">
										<div class="card-body">
											<p class="text-shadow text-center">La socialización nos hace familia, crea lazos para su formación como persona.</p>
										</div>
									</div>
								</div>
								<div class="carousel-item">
									<img class="d-block w-100" src="images/img-bachiller-ciencias/estudiantes-en-biomuseo.jpg" alt="Estudiantes en el biomuseo de Panamá">
									<div class="card">
										<div class="card-body">
											<p class="text-shadow text-center">Visita de estudiantes al museo de la biodiversidad en donde reciben información.</p>
										</div>
									</div>
								</div>
								<div class="carousel-item">
									<img class="d-block w-100" src="images/img-bachiller-ciencias/estudiantes-en-excursion.jpg" alt="Laboratorio Marino Punta Galeta">
									<div class="card">
										<div class="card-body">
											<p class="text-shadow text-center">Visita de estudiantes al centro de investigación y Laboratorio Marino Punta Galeta.</p>
										</div>
									</div>
								</div>
								<div class="carousel-item">
									<img class="d-block w-100" src="images/img-bachiller-ciencias/estudiantes-en-excursion-2.jpg" alt="Visita a manglares y ecosistemas">
									<div class="card">
										<div class="card-body">
											<p class="text-shadow text-center">Visita a manglares, ecosistemas marinos y costeros de la costa caribeña de Panamá.</p>
										</div>
									</div>
								</div>
							</div>
							<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
						<div class="carousel-item">
							<img src="images/img-bachiller-ciencias/laboratorio-punta-galeta.jpg" alt="...">
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid mb-4 text-gray">
			<div class="container">
				<h2 class="my-4 ">Semana de la Ciencia</h2>
				<h3>Durante esta semana compartimos experiencias, ideas, proyectos, artes escénicas desde niveles de Premedia y Media. </h3>
				<div class="row mt-5">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 my-auto">
						<h4>Una pequeña muestra del arte y el conocimiento, en donde el joven da a conocer sus ideas y con relación a las ciencias, desarrollando múltiples competencias:</h4>
						<ul id="ul-ciencia">
							<li class="li-ciencia">Acciones que van en contra de su propio medio</li>
							<li class="li-ciencia">Aplicación del método científico</li>
							<li class="li-ciencia">Convivencia sana y disciplinada</li>
							<li class="li-ciencia">Reafirmación de los contenidos</li>
							<li class="li-ciencia">Trabajo en grupo</li>
						</ul>
						
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<div class="container-fluid">
							<img class="img-fluid img-shadow" src="images/img-bachiller-ciencias/bachiller-en-ciencias.jpg" >
							<p id="text-cohetes">Lanzamiento de cohetes.</p>
						</div>
						
					</div>
				</div>
				<div class="row py-4">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<img class="img-fluid img-shadow mb-3" src="images/img-bachiller-ciencias/semana-de-la-ciencia-2.jpg" >
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<img class="img-fluid img-shadow mb-3" src="images/img-bachiller-ciencias/exposicion-de-proyectos.jpg" >
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<img class="img-fluid img-shadow" src="images/img-bachiller-ciencias/semana-de-la-ciencia.jpg" >	
					</div>
					<div class="container">
					<p class="py-3 text-center">Las competencias son parte del diario vivir, ellas implican comprobar el nivel de conocimiento que poseen.</p>	
					</div>					
				</div>
			</div>
		</div>
		<div class="container-fluid fondo_seccion_primary">
			<div class="container">
				<h2 class="my-4">Lúdica Química en la Cocina</h2>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<img class="img-fluid img-shadow mb-3" src="images/img-bachiller-ciencias/quimica-cocina-3.jpg" >
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<p>Al estudiante se le enseña la aplicación de los distintos procesos químicos que se dan en la preparación de los alimentos, para comprender el por qué de diversas situaciones que se presentan al cocinar.</p>
						<p>La cocina es arte y mucha ciencia a la vez. En este proceso también se promueve mucho la búsqueda de la información, el análisis y uso de lenguaje técnico para la comunicación de ideas y explicaciones.</p>
						
					</div>
				</div>
				<div class="row my-3">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 my-auto">
						<p>Química en la cocina implica física, matemáticas y biología, el alumno debe manejar cantidades, trasformar en otras unidades, afectaciones y ventajas en el comer, estructuras biológicas, entre otros saberes que le permiten una mejor comprensión del medio y del como alimentarse de forma saludable.</p>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<img class="img-fluid img-shadow" src="images/img-bachiller-ciencias/quimica-cocina-2.jpg" >
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<!-- Inicio footer-->
			<div class="row footer">
				<?php include ("sections/footer.html");?>
			</div>
		</div>
		<script src="js/jquery.js"></script>
		<script src="lib/parallax.js-1.5.0/parallax.min.js"></script>
		<script src="lib/bootstrap/js/bootstrap.min.js"></script>
		
	</body>
</html>