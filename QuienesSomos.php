<!DOCTYPE html>
<html lang = "es">
  
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="styles/lib/bootstrap.min.css">
    <link rel="stylesheet" href="styles/index_css.css">
    <link rel="stylesheet" href="resources/fontawesome/css/all.css">
    <link rel="stylesheet" type="text/css" href="styles/all.css"> 
    <link rel="stylesheet" href="styles/estilo_historia.css">

<!--Link para efectos de scroll y animación-->
     <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet" />
    <!-- BOOTSTRAP 4 -->
    <!-- SCROOLL REVEAL JS LIBRARY CDN -->
    <script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>
    <!-- CUSTOM CSS -->
    <link rel="stylesheet" href="styles/main.css">

   <title>¿Quiénes Somos?</title>

</head>

<body>

    <div class="container-fluid">
      <div class="row">
        <?php include ("sections/menu.html");?>
      </div>
    </div>

  <div class="container card">
    

      <!-- Start Contacto-->
      
      <?php include "sections/cont_quienessomos.html"?>

<div class="row footer">
  <?php include "sections/footer.html"?>
</div>
      



    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
     <!-- SCRIPTS -->

    <!-- SCROOLL REVEAL SCRIPT -->
    <script>
      window.sr = ScrollReveal();

    sr.reveal('.navbar', {
      duration: 2000,
      origin: 'bottom'
    });

    sr.reveal('.header-content-left', {
      duration: 2000,
      origin: 'top',
      distance: '300px'
    });

    sr.reveal('.header-content-right', {
      duration: 2000,
      origin: 'right',
      distance: '300px'
    });

    sr.reveal('.header-btn', {
      duration: 2000,
      delay: 1000, 
      origin: 'bottom'
    });

    sr.reveal('#testimonial div', {
      duration: 2000,
      origin: 'left',
      distance: '300px',
      viewFactor: 0.2
    });

    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
      anchor.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
          behavior: 'smooth'
        });
      });
    });
    </script>

  </div>

</body>
</html>
