-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 15, 2019 at 05:26 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devconte21`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
CREATE TABLE IF NOT EXISTS `blog` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(200) NOT NULL,
  `Extracto` varchar(200) NOT NULL,
  `Fecha` timestamp NOT NULL,
  `Texto` text NOT NULL,
  `Thumb` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `datosacudientescon`
--

DROP TABLE IF EXISTS `datosacudientescon`;
CREATE TABLE IF NOT EXISTS `datosacudientescon` (
  `Nombre` varchar(40) NOT NULL,
  `CedulaAcudiente` int(11) NOT NULL AUTO_INCREMENT,
  `IdPrematricula` int(11) NOT NULL,
  `Telefono` varchar(15) NOT NULL,
  `E_mail` varchar(30) NOT NULL,
  PRIMARY KEY (`CedulaAcudiente`),
  KEY `IdPrematricula` (`IdPrematricula`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `datosprematriculacon`
--

DROP TABLE IF EXISTS `datosprematriculacon`;
CREATE TABLE IF NOT EXISTS `datosprematriculacon` (
  `IdPrematricula` int(11) NOT NULL AUTO_INCREMENT,
  `NFormularios` int(11) NOT NULL,
  `CentroEducativo` varchar(30) NOT NULL,
  `Provincia` varchar(20) NOT NULL,
  `Corregimiento` varchar(30) NOT NULL,
  `OfertaAcademica` varchar(40) NOT NULL,
  `Nivel` int(2) NOT NULL,
  `TipoDeEstudiante` varchar(20) NOT NULL,
  `Beca` varchar(20) DEFAULT NULL,
  `ApoyoSocial` varchar(25) DEFAULT NULL,
  `Discpacidad` varchar(20) DEFAULT NULL,
  `Habilidad` text,
  PRIMARY KEY (`IdPrematricula`),
  KEY `NFormularios` (`NFormularios`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `estudiantescon`
--

DROP TABLE IF EXISTS `estudiantescon`;
CREATE TABLE IF NOT EXISTS `estudiantescon` (
  `NFormulario` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Cedula` varchar(15) NOT NULL,
  `ApellidoPaterno` varchar(20) DEFAULT NULL,
  `ApellidoMaterno` varchar(20) NOT NULL,
  `PrimerNombre` varchar(20) NOT NULL,
  `SegundoNombre` varchar(20) DEFAULT NULL,
  `Sexo` char(1) NOT NULL,
  `FechaNacimiento` date NOT NULL,
  `Provincia` varchar(20) NOT NULL,
  `Distrito` varchar(20) NOT NULL,
  `Corregimineto` varchar(20) NOT NULL,
  `Comunidad` varchar(20) NOT NULL,
  `Telefono` varchar(15) NOT NULL,
  PRIMARY KEY (`NFormulario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `estudiantescon`
--

INSERT INTO `estudiantescon` (`NFormulario`, `Fecha`, `Cedula`, `ApellidoPaterno`, `ApellidoMaterno`, `PrimerNombre`, `SegundoNombre`, `Sexo`, `FechaNacimiento`, `Provincia`, `Distrito`, `Corregimineto`, `Comunidad`, `Telefono`) VALUES
(1, '2019-11-14 18:14:59', '2-985-0987', 'Lopes', 'Perez', 'Chichico', 'Manri', 'M', '2019-02-06', 'Cocle', 'Anton', 'Anton', 'Las guaba', '998-0987');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `cedula` varchar(20) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`cedula`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `datosacudientescon`
--
ALTER TABLE `datosacudientescon`
  ADD CONSTRAINT `datosacudientescon_ibfk_1` FOREIGN KEY (`IdPrematricula`) REFERENCES `datosprematriculacon` (`IdPrematricula`);

--
-- Constraints for table `datosprematriculacon`
--
ALTER TABLE `datosprematriculacon`
  ADD CONSTRAINT `datosprematriculacon_ibfk_1` FOREIGN KEY (`NFormularios`) REFERENCES `estudiantescon` (`NFormulario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION 





CREATE TABLE IF NOT EXISTS `prematriculacon` (
  `NFormulario` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Cedula` varchar(15) NOT NULL,
  `ApellidoPaterno` varchar(20) DEFAULT NULL,
  `ApellidoMaterno` varchar(20) NOT NULL,
  `PrimerNombre` varchar(20) NOT NULL,
  `SegundoNombre` varchar(20) DEFAULT NULL,
  `Sexo` char(1) NOT NULL,
  `FechaNacimiento` date NOT NULL,
  `Provincia` varchar(20) NOT NULL,
  `Distrito` varchar(20) NOT NULL,
  `Corregimineto` varchar(20) NOT NULL,
  `Comunidad` varchar(20) NOT NULL,
  `Telefono` varchar(15) NOT NULL,
   `CentroEducativo` varchar(30) NOT NULL,
  `Provinciac` varchar(20) NOT NULL,
  `Corregimiento` varchar(30) NOT NULL,
  `OfertaAcademica` varchar(40) NOT NULL,
  `Nivel` int(2) NOT NULL,
  `TipoDeEstudiante` varchar(20) NOT NULL,
  `Beca` varchar(20) DEFAULT NULL,
  `ApoyoSocial` varchar(25) DEFAULT NULL,
  `Discpacidad` varchar(20) DEFAULT NULL,
  `Habilidad` text,`Nombre` varchar(40) NOT NULL,
  `CedulaAcudiente` int(11) NOT NULL ,
  `IdPrematricula` int(11) NOT NULL,
  `Telefonoac` varchar(15) NOT NULL,
  `E_mail` varchar(30) NOT NULL,
  PRIMARY KEY (`NFormulario`)
) ENGINE=InnoDB AUTO_INCREMENT=1 CHARSET=latin1;
*/;
