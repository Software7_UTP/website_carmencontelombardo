
<?php
session_start();
class Insertar
{
    private $db;
    private $errores;
    private $enviado;
    private $codigo;
    private $primernombre;
    private $segundononbre;
    private $apellidopaterno;
    private $apellidomaterno;
    private $cedula;
    private $sexo;
    private $fechanacimiento;
    private $provincia;
    private $distrito;
    private $corregimiento;
    private $comunidad;
    private $telefono;
    private $centroeducativo;
    private $distritoplantel;
    private $corregimientoplantel;
    private $comunidadplantel;
    private $ofertaacademica;
    private $nivel;
    private $tipodeestudiante;
    private $beca;
    private $apoyosocial;
    private $discapacidad;
    private $habilidad;
    private $nombreacudinete;
    private $cedulaacudiente;
    private $telefonoacudiente;
    private $correo;







    public function __construct(

        $primernombre,
        $segundononbre,
        $apellidopaterno,
        $apellidomaterno,
        $cedula,
        $sexo,
        $fechanacimiento,
        $provincia,
        $distrito,
        $corregimiento,
        $comunidad,
        $telefono,
        $centroeducativo,
        $distritoplantel,
        $corregimientoplantel,
        $comunidadplantel,
        $ofertaacademica,
        $nivel,
        $tipodeestudiante,
        $beca,
        $apoyosocial,
        $discapacidad,
        $habilidad,
        $nombreacudinete,
        $cedulaacudiente,
        $telefonoacudiente,
        $correo,
        $codigo
    ) {
        require_once("Funciones.php");
        $this->db = conectar::conexion();
        $this->primernombre = $primernombre;
        $this->segundononbre = $segundononbre;
        $this->apellidopaterno = $apellidopaterno;
        $this->apellidomaterno = $apellidomaterno;
        $this->cedula = $cedula;
        $this->sexo = $sexo;
        $this->fechanacimiento = $fechanacimiento;
        $this->provincia = $provincia;
        $this->distrito = $distrito;
        $this->corregimiento = $corregimiento;
        $this->comunidad = $comunidad;
        $this->telefono = $telefono;
        $this->centroeducativo = $centroeducativo;
        $this->distritoplantel = $distritoplantel;
        $this->corregimientoplantel = $corregimientoplantel;
        $this->comunidadplantel = $comunidadplantel;
        $this->ofertaacademica = $ofertaacademica;
        $this->nivel = $nivel;
        $this->tipodeestudiante = $tipodeestudiante;
        $this->beca = $beca;
        $this->apoyosocial = $apoyosocial;
        $this->discapacidad = $discapacidad;
        $this->habilidad = $habilidad;
        $this->nombreacudinete = $nombreacudinete;
        $this->cedulaacudiente = $cedulaacudiente;
        $this->telefonoacudiente = $telefonoacudiente;
        $this->correo = $correo;
        $this->codigo=$codigo;
    }



    public function InsertarDatos()
    {
        $statement = $this->db->prepare('INSERT INTO estudiantescon(Cedula, ApellidoPaterno, ApellidoMaterno, PrimerNombre, SegundoNombre, Sexo, FechaNacimiento, Provincia, Distrito, Corregimiento, Comunidad, Telefono)
        Values(:Cedula,:ApellidoPaterno,:ApellidoMaterno,:PrimerNombre,:SegundoNombre,:Sexo,:FechaNacimiento,:Provincia,:Distrito,:Corregimiento,:Comunidad, :Telefono)');

        $statement->execute(array(
            ':Cedula' => $this->cedula,
            ':ApellidoPaterno' => $this->apellidopaterno,
            ':ApellidoMaterno' => $this->apellidomaterno,
            ':PrimerNombre' => $this->primernombre,
            ':SegundoNombre' => $this->segundononbre,
            ':Sexo' => $this->sexo,
            ':FechaNacimiento' => $this->fechanacimiento,
            ':Provincia' => $this->provincia,
            ':Distrito' => $this->distrito,
            ':Corregimiento' => $this->corregimiento,
            ':Comunidad' => $this->comunidad,
            ':Telefono' => $this->telefono



        ));
        $statement->die();

        return $statement;
    }


    public function InsertarDatos2()
    {
        try{
            $statement = $this->db->prepare('INSERT INTO prematriculacon(Cedula, ApellidoPaterno, ApellidoMaterno, PrimerNombre, SegundoNombre, Sexo, FechaNacimiento, Provincia, Distrito, Corregimineto, Comunidad, Telefono, CentroEducativo, Corregimiento, OfertaAcademica, Nivel, TipoDeEstudiante, Beca, ApoyoSocial, Discapacidad, Habilidad, Nombre, CedulaAcudiente,  Telefonoac, E_mail,codigo)
        Values(:Cedula,:ApellidoPaterno,:ApellidoMaterno,:PrimerNombre,:SegundoNombre,:Sexo,:FechaNacimiento,:Provincia,:Distrito,:Corregimiento,:Comunidad, :Telefono,:CentroEducativo,:Corregimiento, :OfertaAcademica, :Nivel, :TipoDeEstudiante, :Beca, :ApoyoSocial, :Discapacidad, :Habilidad, :Nombre, :CedulaAcudiente,:Telefonoac, :E_mail,:codigo)');

        $statement->execute(array(
            ':Cedula' => $this->cedula,
            ':ApellidoPaterno' => $this->apellidopaterno,
            ':ApellidoMaterno' => $this->apellidomaterno,
            ':PrimerNombre' => $this->primernombre,
            ':SegundoNombre' => $this->segundononbre,
            ':Sexo' => $this->sexo,
            ':FechaNacimiento' => $this->fechanacimiento,
            ':Provincia' => $this->provincia,
            ':Distrito' => $this->distrito,
            ':Corregimiento' => $this->corregimiento,
            ':Comunidad' => $this->comunidad,
            ':Telefono' => $this->telefono,
            ':CentroEducativo' => $this->centroeducativo,
            ':Corregimiento' => $this->corregimiento,
            ':OfertaAcademica' => $this->ofertaacademica,
            ':Nivel' => $this->nivel,
            ':TipoDeEstudiante' => $this->tipodeestudiante,
            ':Beca' => $this->beca,
            ':ApoyoSocial' => $this->apoyosocial,
            ':Discapacidad' => $this->discapacidad,
            ':Habilidad' => $this->habilidad,
            ':Nombre' => $this->nombreacudinete,
            ':CedulaAcudiente' => $this->cedulaacudiente,
            ':Telefonoac' => $this->telefonoacudiente,
            ':E_mail' => $this->correo,
            ':codigo'=>$this->codigo
            




        ));

        

        try{

            $statement = $this->db->prepare('UPDATE codigo SET uso = uso+1 WHERE codigo=:codigo');
            $statement->execute(array(
                ':codigo'=>$this->codigo
             ));


             
             if ($statement->rowCount()>0) {
                 

               
                
    $var = " PRE-MATRICULA REALIZADA CORRECTAMENTE FELICIDADES!";
    echo "<script>
    alert('".$var."');
  
   </script>";
   session_destroy();
   
   header("Location: http://localhost/website_carmencontelombardo/");
   
            }else{
                $var ="ALGO SALIO MAL INTENTELO DE NUEVO, DUSCULPE!";
    echo "<script>
    alert('".$var."');
  
   </script>";

   header("Location: http://localhost/website_carmencontelombardo/pre-inscripcion.php?var=$this->codigo");
   
            }


             



        }catch(Exception $i){


            echo"Mensaje del codigo". $i->getMessage();

        }

        


        
        
    /*
     
     */
        }catch(PDOException $e){
            echo $e->getMessage();
        }
        
        

        
    }

    public function sacarDatos(){
        
    }
}
?>












