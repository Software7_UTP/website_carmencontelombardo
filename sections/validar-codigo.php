<?php 

if (isset($_POST['varificar'])) {
  $email = $_POST['cod'];
}
 ?>

<div class="modal fade" id="dialogo1">
      <div class="modal-dialog">
        <div class="modal-content Mdal">
    
          <!-- cabecera del diálogo -->
          <div class="modal-header">
            <h4 class="modal-title">Verificar Código</h4>
            <button type="button" class="close" data-dismiss="modal">x</button>
          </div>
    
          <!-- cuerpo del diálogo -->
          <div class="modal-body">
            <form role="form">
                    <div class="form-group">
                        <p class="Please">Por favor, ingrese el código que le fue dado por la secretaría del Instituto.</p>
                        <input type="text" class="form-control" name="cod" id="cod" required="" />
                    </div>
                    <button type="submit" class="btn btn-primary" name="verificar">Verificar</button>
                </form>
          </div>
    
          <!-- pie del diálogo -->
          <div class="modal-footer">
            
          </div>
    
        </div>
      </div>
    </div> 