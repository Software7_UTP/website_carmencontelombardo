<!DOCTYPE html>
<html lang="">
	<head>
	
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Noticias</title>
		<!-- Bootstrap CSS -->
		
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" >
		<link rel="stylesheet" href="styles/index_css.css">
		<link rel="stylesheet" href="lib/fontawesome/css/all.css">
		<link rel="stylesheet" href="styles/all.css">

		<h3><title>Noticias</title></h3>
	
		<style type="text/css">
			.panel-body:hover{
				background: #d0dafd;
   		color:#339;
     
			}
				.panel-body{
   	padding: 8px;
   	background: #e8edff;
   	border-bottom: :1px solid #fff;

   		
   	}
   
		</style>
</head>
	<body>
		  <!--Inicia Menú-->
		<div class="row">
				<?php include ("sections/menu.html");?>
				
			</div>
			<div  class="panel panel-default ">
			</div>
		<!--Termina Menú-->
				
 			
 			    
 			     
 			     	<?php 
 			     	 include ("conexion.php");

 			     	 	
 			     	
 			      $query= "SELECT * FROM blog WHERE Estado='publicado' ORDER BY Id desc ";
 			     $resultado = $conexion->query($query);
 			     while($mostrar=$resultado->fetch_assoc()){
                   ?>
                  <div class="container" >

 		<div class="row ">
 		<div class="panel">
 			<div class="panel-body">
 				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
 					   <?php 
 					  $caden=$mostrar ['Thumb'];
 					   
 					  ?>
 					 <img src="<?php echo substr($caden,24);?>"  width="100%" height="150px" vspace="10"  />
 				</div>
 				<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
 					<h3> <?php echo $mostrar['Titulo'];?></h3>
 					<p>
 					  <?php 
 					  $cadena=$mostrar ['Texto'];
 					  echo nl2br(substr($cadena,0,300))."...";?>
 					
 					  </p>
 					  
                    <form action="MostrarNoticia.php" method="GET" role="form" >
                          
                    	 <button type="submit" class="btn btn-primary" name="enviar" value= "<?php
                    	  echo
                    	  
                    	   $mostrar['Id']

                    	   ?>" 
                          >Leer más</button>
                       </div>
                    	   
                    </form>
                   
                      
                    
           
 				</div>
 				
 			</div>

 		</div>
 	</div>

 	</div> 
 	
                  
                   
               </div>
               
              
                  
                
                   <?php
 			     }
				
				
               
 			    ?>	
 			
 	    	<div class="row">
				<?php include ("sections/footer.html");?>
				
			</div>
 
		
	</body>
</html>
