<?php

include '../controlador/UsuarioControlador.php';
include '../helpslogin/helps.php';

session_start();

header('Content-Type: application/json');
$resultado= array();

if($_SERVER["REQUEST_METHOD"]=="POST"){

if (isset($_POST["txtusuario"])&& isset($_POST["txtpassword"])){

    $txtUsuario = validar_campo($_POST["txtusuario"]);
	$txtPassword=validar_campo($_POST["txtpassword"]);

	$resultado=array("estado" => "true");

	if(UsuarioControlador::login($txtUsuario,$txtPassword)) {
	
		$usuario= UsuarioControlador::getUsuario($txtUsuario, $txtPassword);
		$_SESSION["usuario"]=array(
			"id"=> $usuario->getId(),
			"usuario"=> $usuario->getUsuario(),	
			"apellido"=> $usuario->getApellido(),	
			"correo"=> $usuario->getCorreo(),
			"telefono"=> $usuario->getTelefono(),	
			"tipodeusuario"=> $usuario->getTipodeusuario(),
			);
				return print(json_encode($resultado));	
				};	
}
}



$resultado=array("estado" => "false");

return print(json_encode($resultado)) ;
	

