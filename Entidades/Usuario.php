<?PHP


class Usuario{
	private  $id;
	private $usuario;
	private $apellido;
	private $password;
	private $correo;
	private $telefono;
	private $tipodeusuario;

		public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getUsuario(){
		return $this->usuario;
	}

	public function setUsuario($usuario){
		$this->usuario = $usuario;
	}

	public function getApellido(){
		return $this->apellido;
	}

	public function setApellido($apellido){
		$this->apellido = $apellido;
	}

	public function getPassword(){
		return $this->password;
	}

	public function setPassword($password){
		$this->password = $password;
	}

	public function getCorreo(){
		return $this->correo;
	}

	public function setCorreo($correo){
		$this->correo = $correo;
	}

	public function getTelefono(){
		return $this->telefono;
	}

	public function setTelefono($telefono){
		$this->telefono = $telefono;
	}

	public function getTipodeusuario(){
		return $this->tipodeusuario;
	}

	public function setTipodeusuario($tipodeusuario){
		$this->tipodeusuario = $tipodeusuario;
	}

}
?>