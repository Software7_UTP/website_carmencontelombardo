
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>Starter Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
    	<link rel="stylesheet" type="text/css" href="vista/assets/css/overhang.min.css" />
		
		<script defer src="https://use.fontawesome.com/releases/v5.11.2/js/all.js"></script>
  		<script defer src="https://use.fontawesome.com/releases/v5.11.2/js/v4-shims.js"></script>
		<link rel="stylesheet" type="text/css" href="styles/InicioSesion.css">
		<link rel="stylesheet" href="lib/fontawesome/css/all.css">


  </head>

  <body>

  
		<div class="container">
			<div class="modal-dialog text-center">
				
				<div class="col-sm-8 main-section">
					
					<div class="modal-content">

						<div class="col-12 user-img">
						<img src="images/usuaLogin.jpg">
						</div>
						
						<form class="col-12" id="loginForm" method="POST" action="vista/validar.php">
							
							<div class="form-group">
								<input type="text" name="txtusuario" class="form-control"  placeholder="Usuario" id="usuario" autofocus required="required">
							</div>
							<div class="form-group">
								<input type="password" name="txtpassword" class="form-control" placeholder="password" id="password" required="required">
							</div>
							<button type="submit" class="btn" name="btnSesion"><i class="fas fa-sign-in-alt"></i>Ingresar</button>

							<div class="col-12 forgot">
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#dialogo1" name="recuperar">¿Olvidó su contraseña?</button>
							</div>
					</div>
					
					
						</form>

						

				</div>

			</div>
			
		</div>

		


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>


    

    <script type="text/javascript" src="vista/assets/js/overhang.min.js"></script>
    <script src="vista/assets/js/app.js"></script>
  </body>
</html>
