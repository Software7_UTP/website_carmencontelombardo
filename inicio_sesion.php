<?php
 if (isset($_POST['btnSesion'])) {
	$usuario = $_POST['txtUsuario'];
	$contrasena = $_POST['txtContrasena'];
 } 
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Inicio Sesión</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="fontawesome/css/all.css">
		<script defer src="https://use.fontawesome.com/releases/v5.11.2/js/all.js"></script>
  		<script defer src="https://use.fontawesome.com/releases/v5.11.2/js/v4-shims.js"></script>
		<link rel="stylesheet" type="text/css" href="styles/InicioSesion.css">
		<link rel="stylesheet" href="lib/fontawesome/css/all.css">
	</head>
	<body>
		

		<div class="container"><!--Inicio de container-->
			<div class="modal-dialog text-center">
				
				<div class="col-sm-8 main-section"><!--Inicio de columna-->

					
					<div class="modal-content"><!--Inicio modal content-->

						<div class="col-12 user-img"><!--Div para insertar la imagen-->
						<img src="images/usuaLogin.jpg">
						</div>
						
						<form class="col-12" method="POST" action="login/validar_login.php">
							
							<div class="form-group">
								<input type="text" name="txtUsuario" class="form-control"  placeholder="Usuario" id="usuario"  required="required">
							</div>
							<div class="form-group">
								<input type="password" name="txtPassword" class="form-control" placeholder="password" id="password" required="required">
							</div>
							<button type="submit" class="btn" name="btnSesion"><i class="fas fa-sign-in-alt"></i>Login</button>

							
							<div class="col-12 forgot">
								<!--
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#dialogo1" name="recuperar">¿Olvidó su contraseña?</button>
								-->
							</div>

						
						</form>	
					</div><!--Final modal content-->

						<?php 
							//include 'recuperar_contrasena.php'; //Se llama al formulario de recuperarcontraseña 
						 ?>

				</div><!--Fin de columna-->

			</div>
			
		</div><!--Fin de container-->

		



		

		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

		<script type="text/javascript" language="javascript" src="scriptaculous/lib/prototype.js"></script>
<script type="text/javascript" language="javascript" src="scriptaculous/src/scriptaculous.js"></script>
	</body>
</html>