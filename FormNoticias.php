<!DOCTYPE html>
<html>
<head>
	<title>Formulario de Noticias</title>
	<script type="text/javascript" src="js/jquery-1.12.0.js"></script>
	<link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
    
	<link rel="stylesheet" type="text/css" href="styles/noticiadmin.css">
	
	
  <script type="text/javascript">
		function caracter_tiulo(obj,maxLenght){
		if(obj.value.length>maxLenght){
				obj.value=obj.value.substr(0,maxLenght);
					alert("¡ATENCIÓN!\n"+"Sobrepaso la cantidad máxima de caracteres permitos\n"
										  +"Cantidad de caracteres permitidos :"+maxLenght+" caracteres\n"
										  +"Se ha a borrado los caracteres finales");
			}
			
			return false;	
		}

	</script>
	<script type="text/javascript">
		function d(obj,maxLenght){
		if(obj.value.length>maxLenght){
				obj.value=obj.value.substr(0,maxLenght);
					alert("¡ATENCIÓN!\n"+"Sobrepaso la cantidad máxima de caracteres permitos\n"
										  +"Cantidad de caracteres permitidos :"+maxLenght+" caracteres\n"
										  +"Se ha a borrado los caracteres finales");
			}
			
			return false;	
		}

	</script>
</head>
<body>
	
	
	<div class="container">
	
		<div class="row">
			<p>
			<h1>FORMULARIO DE NOTICIAS</h1>
			<p>
			<div class="col-sm-12">
				<form action="FormNoticias.php" method="post " enctype="multipart/form-data" >
					
					<div class="form-group">
						<legend>Inserte el título de la noticia</legend>
						<input type="text" name="titulo" id="titulo" class="form-control" onkeyup="caracter_tiulo(this,100)" onmouseover="caracter_tiulo(this,100)" required="required"  >
					</div>
					<div class="form-group">
						
						<legend>Inserte la imagen de la noticia</legend>
						<input type="file" name="imagen" id="imagen" class="form-control" value="" required="required" accept="image/*" >
					</div>
					<div class="form-group">
						
						<legend>Inserte el detalle de la noticia</legend>
						
					 
	            <textarea name="detalle" onkeyup="d(this,1500)" onmouseover="d(this,1500)" required>
	            
		           
	                 
	                  </textarea>
						
					
						
					</div>
					<div class="form-group">
				   
					<input type="submit" class="btn btn-primary" name="enviar" value="Enviar">
						</div>
				</form>

			</div>
		</div>
		
	</div>
</body>
</html>

<?php
include("Funcion.php");
if(isset($_POST['enviar'])){
if($_FILES['imagen']['size']>2024000){
	echo"<script language='javascript'> alert('imagen menor a 2mb')</script>";
	exit;
}

$titulo=$_POST["titulo"];
$noticia= $_POST["detalle"];
$dir="C:/wampp64/www/website_carmencontelombardo/imagenes/";
$imagen= $_FILES['imagen']['name'];
if (!move_uploaded_file( $_FILES['imagen']['tmp_name'],$dir.$imagen)){
echo "error";
}


$objeto = new Noticias($titulo,$noticia,$dir,$imagen);
$objeto->insertar();
}
?>

