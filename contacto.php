<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Librerías y estilos usados-->
		<link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="lib/fontawesome/css/all.css">
		<link rel="stylesheet" href="styles/all.css">
		<!-- Título de la página-->
		<title>Contacto</title>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<!-- barra de navegacion-->
				<?php include ("sections/menu.html");?>
			</div>
		</div>
		<!-- Titulo de la pagina en forma de banner-->
			  <div class="parallax" data-parallax="scroll" data-image-src="images/bg-titles-page.png">
      <h1 class="parallax-title text-center py-5 text-shadow"><b>CONTACTO</b></h1>
    </div><br>
			<div class="container-fluid">	

<!-- Inicio Contacto-->
			<div class="row">
				<!--Contenido de contacto -->
				<?php include "sections/contenido_contacto.html"?>

			</div>
			<br>
			<!-- Inicio Footer-->
			<div class="row footer">
				<?php include ("sections/footer.html");?>
			</div>
		
		</div>
		<!-- Librerías jquery y java script -->
		<script src="js/jquery.js"></script>
  <script src="lib/parallax.js-1.5.0/parallax.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>